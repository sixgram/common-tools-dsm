ifneq ($(TARGET_SIMULATOR),true)

LOCAL_PATH := $(call my-dir)

include $(CLEAR_VARS)

LOCAL_SRC_FILES := \
		dsm_ctrl.c

LOCAL_C_INCLUDES := $(LOCAL_PATH)/include 
LOCAL_MODULE := dsm_ctrl
LOCAL_MODULE_PATH := $(TARGET_OUT_OPTIONAL_EXECUTABLES)
LOCAL_MODULE_TAGS := optional
LOCAL_SHARED_LIBRARIES := libcutils liblog libc

include $(BUILD_EXECUTABLE)

endif
