
/*DSM set/get parameter main function*/

#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <fcntl.h>


static void help(void) __attribute__ ((noreturn));

static void help(void)
{
	fprintf(stderr,
		"Usage: dsm_ctrl [-r] [-w] [-a] [-b] ID [VALUE] \n"
		"  ID is the parameter ID number \n"
		"  VALUE is an integer value for write mode \n"
		"  OPTION is one of:\n"
		"    w (set parameter data)\n"
		"    r (read parameter data)\n"
		"    a (set parameter of left channel for stereo)\n"
		"    b (set parameter of right channel for stereo)\n");
	exit(1);
}

#ifndef TO_FIX
#define TO_FIX(a, q)     					((int)((a) * ((unsigned int)1<<(q))))
#endif

#ifndef TO_DBL
#define TO_DBL(a, q)     					(double)((double)((double)(a)/(double)((unsigned int)1<<(q))))
#endif

#define DSM_API_SETGET_PARAMS_COUNT     (150)


typedef enum {
    DSM_API_MONO_SPKER                  = 0x00000000,//the mono speaker
    DSM_API_STEREO_SPKER                = 0x03000000,//the stereo speakers

    DSM_API_L_CHAN                      = 0x01000000,//the left channel speaker Id
    DSM_API_R_CHAN                      = 0x02000000,//the left channel speaker Id

    DSM_API_CHANNEL_1                   = 0x01000000,
    DSM_API_CHANNEL_2                   = 0x02000000,
    DSM_API_CHANNEL_3                   = 0x04000000,
    DSM_API_CHANNEL_4                   = 0x08000000,
    DSM_API_CHANNEL_5                   = 0x10000000,
    DSM_API_CHANNEL_6                   = 0x20000000,
    DSM_API_CHANNEL_7                   = 0x40000000,
    DSM_API_CHANNEL_8                   = 0x80000000,

    DSM_MAX_SUPPORTED_CHANNELS          = 8
} DSM_API_CHANNEL_ID;


#define DSM_SET_MONO_PARAM(cmdId)       ((cmdId&0x00FFFFFF)|DSM_API_MONO_SPKER)
#define DSM_SET_STEREO_PARAM(cmdId)     ((cmdId&0x00FFFFFF)|DSM_API_STEREO_SPKER)
#define DSM_SET_LEFT_PARAM(cmdId)       ((cmdId&0x00FFFFFF)|DSM_API_L_CHAN)
#define DSM_SET_RIGHT_PARAM(cmdId)      ((cmdId&0x00FFFFFF)|DSM_API_R_CHAN)

struct param_info {
    char name[80];
    uint32_t qVal;
};


static struct param_info pInfos[150] = {
    //The following messages are read/write
    {"DSM_API_GET_MAXIMUM_CMD_ID",                  0},     //32-bits data,                 Q0
    {"DSM_API_SETGET_ENABLE",                       0},     //32-bits data,                 Q0
    {"DSM_API_SETGET_COILTEMP_THRESHOLD",          19},     //32-bits data, in C degrees,   Q19
    {"DSM_API_SETGET_XCL_THRESHOLD",               27},	    //32-bits data, in mm,          Q27
    {"DSM_API_SETGET_LIMITERS_RELTIME",            30},	    //32-bits data, in seconds,     Q30
    {"DSM_API_SETGET_MAKEUP_GAIN",                 29},	    //32-bits data, in %,			Q29
    {"DSM_API_SETGET_RDC_AT_ROOMTEMP",             27},	    //32-bits data,                 Q27
    {"DSM_API_SETGET_COPPER_CONSTANT",             30},	    //32-bits data,                 Q30
    {"DSM_API_SETGET_COLDTEMP",                    19},	    //32-bits data, 				Q19
    {"DSM_API_SETGET_PITONE_GAIN",                 31},	    //32-bits data, in %,			Q31
    {"DSM_API_SETGET_LEAD_RESISTANCE",             27},	    //32-bits data, 				Q27
    {"DSM_API_SETGET_HPCUTOFF_FREQ",                9},	    //32-bits data, in Hz,			Q9
    {"DSM_API_SETGET_LFX_GAIN",                    30},	    //32-bits data, 				Q30

    //for impedence model updating
    {"DSM_API_SETGET_REF_FC",                       9},     //32-bits data, in Hz,			Q9
    {"DSM_API_SETGET_REF_Q",                       29},     //32-bits data, 				Q29
    {"DSM_API_INIT_F_Q_FILTERS",                    0},     //32-bits data, 				Q0

    //The following messages are read only
    {"DSM_API_GET_ADAPTIVE_FC",                     9},     //32-bits data, in Hz,			Q9
    {"DSM_API_GET_ADAPTIVE_Q",                     29},     //32-bits data, 				Q29
    {"DSM_API_GET_ADAPTIVE_DC_RES",                27},     //32-bits data, 				Q27
    {"DSM_API_GET_ADAPTIVE_COILTEMP",              19},     //32-bits data, 				Q19
    {"DSM_API_GET_EXCURSION",                      27},     //32-bits data, 				Q27

    //The fllowing 4 commands are used the buffers used for the following commands must be allocated
    //by the caller function. The maxim buffer size is 4K bytes.
    {"DSM_API_GET_PCM_INPUT_DATA",                  0},     //the buffer will be filled with 16-bit input PCM data to DSM module.
    {"DSM_API_GET_IV_DATA",                         0},     //the buffer will be filled with I and V data.
    {"DSM_API_GET_PCM_AND_IV_DATA",                 0},     //the buffer will be filled with PCM, I and V data. All data are in 16-bit integer.
    {"DSM_API_GET_PCM_OUTPUT_DATA",                 0},     //the buffer will be filled with 16-bit output PCM data of DSM module.

    {"DSM_API_SETGET_INTERN_DEBUG",                 0},     //This command is obsolete.
    {"DSM_API_SETGET_VLIMIT",                      27},     //32-bits data, 				Q27
    {"DSM_API_SETGET_NEW_CONTROLS",                 0},     //This command is obsolete.

    {"DSM_API_SETGET_PILOT_ENABLE",                 0},     //32-bits data, 				Q0
    {"DSM_API_SETGET_CLIP_ENABLE",                  0},     //32-bits data, 				Q0
    {"DSM_API_SETGET_EXC_ENABLE",                   0},     //32-bits data, 				Q0
    {"DSM_API_SETGET_THERMAL_ENABLE",               0},     //32-bits data, 				Q0

    {"DSM_API_SETGET_EQ_BAND_FC",                   9},   //32-bits data, in Hz,          Q9
    {"DSM_API_SETGET_EQ_BAND_Q",                   29},   //32-bits data,                 Q29
    {"DSM_API_SETGET_EQ_BAND_ATTENUATION_DB",      20},   //32-bits data, in db,          Q20
    {"DSM_API_SETGET_TARGET_EQ_BAND_ID",            0},   //32-bits data,                 Q0
    {"DSM_API_SETGET_EQ_BAND_ENABLE",               0},   //32-bits data,                 Q0. Bit field defintions: bit0:band1,bit1:band2,bit2:band3,bit3:band4 
    {"DSM_API_GET_EQ_BAND_COEFFICIENTS",            0},   //This command is obsolete.

    // ZIMP - imdepdence filter coefficients, for power from voltage
    {"DSM_API_GET_ZIMP_A1",                        28},   //32-bits data,                 Q28
    {"DSM_API_GET_ZIMP_A2",                        28},   //32-bits data,                 Q28
    {"DSM_API_GET_ZIMP_B0",                        28},   //32-bits data,                 Q28
    {"DSM_API_GET_ZIMP_B1",                        28},   //32-bits data,                 Q28
    {"DSM_API_GET_ZIMP_B2",                        28},   //32-bits data,                 Q28

    // EQ Biquad coefficients	
    {"DSM_API_SETGET_EQ1_A1",                      28},   //32-bits data, 				Q28
    {"DSM_API_SETGET_EQ1_A2",                      28},   //32-bits data, 				Q28
    {"DSM_API_SETGET_EQ1_B0",                      28},   //32-bits data, 				Q28
    {"DSM_API_SETGET_EQ1_B1",                      28},   //32-bits data, 				Q28
    {"DSM_API_SETGET_EQ1_B2",                      28},   //32-bits data, 				Q28

    {"DSM_API_SETGET_EQ2_A1",                      28},   //32-bits data,                 Q28
    {"DSM_API_SETGET_EQ2_A2",                      28},   //32-bits data,                 Q28
    {"DSM_API_SETGET_EQ2_B0",                      28},   //32-bits data,                 Q28
    {"DSM_API_SETGET_EQ2_B1",                      28},   //32-bits data,                 Q28
    {"DSM_API_SETGET_EQ2_B2",                      28},   //32-bits data,                 Q28

    {"DSM_API_SETGET_RDC_SCALING",                 25},   //32-bits data,                 Q28
    {"DSM_API_SETGET_MBDRC_TARGET_SUBBAND_ID",      0},   //32-bits data,                 Q28
    {"DSM_API_SETGET_MBDRC_ENABLE",                 0},   //32-bits data,                 Q28
    {"DSM_API_SETGET_DRC_TRHESHOLD",               20},   //32-bits data,                 Q28
    {"DSM_API_SETGET_DRC_RATIO",                   20},   //32-bits data,                 Q28

    {"DSM_API_SETGET_DRC_ATTACK",                  20},   //32-bits data,                 Q28
    {"DSM_API_SETGET_DRC_RELEASE",                 20},   //32-bits data,                 Q28
    {"DSM_API_SETGET_MBDRC_CUTOFF_F1",              0},   //32-bits data,                 Q28
    {"DSM_API_SETGET_MBDRC_CUTOFF_F2",              0},   //32-bits data,                 Q28
    {"DSM_API_SETGET_MBDRC_DEBUG",                  0},   //32-bits data,                 Q28

    {"DSM_API_SETGET_TRAJECTORY",                   0},   //32-bits data,                 Q0
    {"DSM_API_SETGET_GUARD_BEEN_MEAN_SCALE",        30},   //32-bits data,                 Q0

    {"DSM_API_SET_UPDATE_DELAY",                    0},   //32-bits data,                 Q0
    {"DSM_API_SETGET_DELAY",                        0},   //32-bits data,                 Q0
    {"DSM_API_GET_MAXIMUM_DELAY",                   0},   //32-bits data,                 Q0

    {"DSM_API_SETGET_ENABLE_FAST_FC",               0},   //32-bits data,                 Q0

    {"DSM_API_SETGET_ORM_QUALIFY_THRESH",           30},   //32-bits data,                 Q0

    {"DSM_API_SETGET_ENABLE_LOGGING",               0},   //32-bits data,                 Q0

    {"DSM_API_SETGET_SPEAKER_PARAM_LFX_A1",        28},   //32-bits data,                 Q28
    {"DSM_API_SETGET_SPEAKER_PARAM_LFX_A2",        28},   //32-bits data,                 Q28
    {"DSM_API_SETGET_SPEAKER_PARAM_LFX_B0",        28},   //32-bits data,                 Q28
    {"DSM_API_SETGET_SPEAKER_PARAM_LFX_B1",        28},   //32-bits data,                 Q28
    {"DSM_API_SETGET_SPEAKER_PARAM_LFX_B2",        28},   //32-bits data,                 Q28
    {"DSM_API_SETGET_SPEAKER_PARAM_TCTH1",         20},   //32-bits data,                 Q20
    {"DSM_API_SETGET_SPEAKER_PARAM_TCTH2",         20},   //32-bits data,                 Q20
    {"DSM_API_SETGET_SPEAKER_PARAM_RTH1",          22},   //32-bits data,                 Q22
    {"DSM_API_SETGET_SPEAKER_PARAM_RTH2",          22},   //32-bits data,                 Q22
    {"DSM_API_SETGET_SPEAKER_PARAM_ADMIT_A1",      28},   //32-bits data,                 Q28
    {"DSM_API_SETGET_SPEAKER_PARAM_ADMIT_A2",      28},   //32-bits data,                 Q28
    {"DSM_API_SETGET_SPEAKER_PARAM_ADMIT_B0",      28},   //32-bits data,                 Q28
    {"DSM_API_SETGET_SPEAKER_PARAM_ADMIT_B1",      28},   //32-bits data,                 Q28
    {"DSM_API_SETGET_SPEAKER_PARAM_ADMIT_B2",      28},   //32-bits data,                 Q28

    {"DSM_API_SETGET_SPEAKER_PARAM_UPDATE",         0},   //unsigned 32-bit,              Q0

    {"DSM_API_SETGET_READBACK_EQ_BAND_ID",          0},   //unsigned 32-bit,              Q0

    {"DSM_API_SETGET_GENERATE_SINE_WAVE",           0},   //32-bit,                       Q0

    // special APIs
    {"DSM_API_GET_FIRMWARE_BUILD_TIME",             0},   //C string
    {"DSM_API_GET_FIRMWARE_BUILD_DATE",             0},   //C string
    {"DSM_API_GET_FIRMWARE_VERSION",                0},   //C string
    {"DSM_API_GET_CHIPSET_MODEL",                   0},   //C string
    {"DSM_API_GET_ENDIAN",                          0},   //32-bits data = 0xdeadbeef,    Q0

    {"DSM_API_SETGET_SINE_WAVE_GAIN",              15},   //32-bits data,                 Q15

    {"DSM_API_SETGET_VIRTUAL_V_ENABLE",             0},   //32-bits data,                 Q0
    {"DSM_API_SETGET_FORCED_VIRTUAL_V",             0},   //32-bits data,                 Q0
    {"DSM_API_SETGET_I_SHIFT_BITS",                 0},   //32-bits data,                 Q0

    {"DSM_API_SETGET_EXC_FUNC_GAIN_ADJUSTED",       0},   //32-bits data,                 Q0

    {"DSM_API_SETGET_FADE_IN_TIME_MS",              0},   //32-bits data,                 Q0
    {"DSM_API_SETGET_FADE_OUT_TIME_MS",             0},   //32-bits data,                 Q0

    //This command takes effect only after
    //DSM_API_FADE_OUT_TIME_MS
    {"DSM_API_SETGET_FADE_OUT_MUTE_TIME_MS",        0},  //32-bits data,                 Q0

    {"DSM_API_SETGET_FADE_IN_ENABLE",               0},  //32-bits data,                 Q0
    {"DSM_API_SETGET_FADE_OUT_ENABLE",              0},  //32-bits data,                 Q0

    {"DSM_API_SETGET_ENABLE_MULTICHAN_LINKING",     0},  //32-bit data,                  Q0
    {"DSM_API_SETGET_ENABLE_SMART_PT",              0},  //32-bit data,                  Q0
    {"DSM_API_SETGET_PILOTTONE_SILENCE_THRESHOLD",            0},  //32-bit data,                  Q0
    {"DSM_API_SETGET_SILENCE_PILOTTONE_GAIN_Q31",            31},  //32-bit data,                  Q0
    {"DSM_API_SETGET_SILENCE_FRAMES",            0},  //32-bit data,                  Q0
    {"DSM_API_SETGET_PILOTTONE_TRANSITION_FRAMES",            0},  //32-bit data,                  Q0
    {"DSM_API_SETGET_PILOTTONE_SMALL_SIGNAL_THRESHOLD",       0},  //32-bit data,                  Q0
    {"DSM_API_SETGET_SMALL_SIGNAL_PILOTTONE_GAIN_Q31",       31},  //32-bit data,                  Q0
    {"DSM_API_SETGET_ENABLE_LINKWITZ_EQ",            0},  //32-bit data,                  Q0
    {"DSM_API_SETGET_CHANNEL_MASK",            0},  //32-bit data,                  Q0
    {"DSM_API_SETGET_ENABLE_FF_FB_MODULES",            0},  //32-bit data,                  Q0
    {"DSM_API_SETGET_ENABLE_CROSSOVER",            0},  //32-bit data,                  Q0
    {"DSM_API_SETGET_AUX_EQ_BAND_FC",            9},  //32-bit data,                  Q0
    {"DSM_API_SETGET_AUX_EQ_BAND_Q",            29},  //32-bit data,                  Q0
    {"DSM_API_SETGET_AUX_EQ_BAND_ATTENUATION_DB",            20},  //32-bit data,                  Q0
    {"DSM_API_SET_EQ_AUX_BAND_COEFF_UPDATE",            0},  //32-bit data,                  Q0
    {"DSM_API_SETGET_AUX_EQ_BAND_ENABLE",            0},  //32-bit data,                  Q0
    {"DSM_API_SETGET_ENABLE_AUX_CROSSOVER",            0},  //32-bit data,                  Q0
    {"DSM_API_SETGET_ENABLE_AUX1_DELAYED_SAMPLES",            0},  //32-bit data,                  Q0
    {"DSM_API_SETGET_ENABLE_AUX2_DELAYED_SAMPLES",            0},  //32-bit data,                  Q0
    {"DSM_API_SETGET_ENABLE_AUX3_DELAYED_SAMPLES",            0},  //32-bit data,                  Q0
    {"DSM_API_SETGET_ENABLE_AUX4_DELAYED_SAMPLES",            0},  //32-bit data,                  Q0
    {"DSM_API_SETGET_SPEECH_GUARD_BINS",            0},  //32-bit data,                  Q0
    {"DSM_API_SETGET_MEAN_SPEECH_THRESHOLD",            31},  //32-bit data,                  Q0
    {"DSM_API_SETGET_HPCUTOFF_FREQ_AUX",            9},  //32-bit data,                  Q0
    {"DSM_API_SETGET_AUX_HP_FILTER_ENABLE",            0},  //32-bit data,                  Q0
    {"DSM_API_SETGET_STEREO_CROSSOVER_MODE",            0},  //32-bit data,                  Q0
    {"DSM_API_SETGET_ENABLE_UPDATE_FC_Q",            0},  //32-bit data,                  Q0
    {"DSM_API_SETGET_RECEIVER_PHYSICAL_LAYOUT",            0},  //32-bit data,                  Q0
    {"DSM_API_SETGET_MAX_V_SAMPLE",            0},  //32-bit data,                  Q0
    {"DSM_API_SETGET_MAX_I_SAMPLE",            0},  //32-bit data,                  Q0
    {"DSM_API_SETGET_MAX_INPUT_SAMPLE",            0},  //32-bit data,                  Q0
    {"DSM_API_SETGET_FB_SKIPPING_MS",            0},  //32-bit data,                  Q0
    {"DSM_API_GET_ADAPTIVE_PARAMS",            0},  //32-bit data,                  Q0
    {"DSM_API_GET_ADAPTIVE_STATISTICS",            0},  //32-bit data,                  Q0
    {"DSM_API_SETGET_SILENCE_UNMUTED_IN_SPT",            0},  //32-bit data,                  Q0
    {"DSM_API_SETGET_Q_ADJUSTMENT",            29},  //32-bit data,                  Q0
    {"DSM_API_SETGET_XOVER_MIXING_ENABLED",            0},  //32-bit data,                  Q0
    {"DSM_API_SETGET_XOVER_BOOST_GAIN_PERCENTAGE",            27},  //32-bit data,                  Q0
    {"DSM_API_SETGET_XOVER_FILTER_CUTOFF_FC",            9},  //32-bit data,                  Q0
    {"DSM_API_SETGET_XOVER_FILTER_Q",           29},  //32-bit data,                  Q0
    {"DSM_API_SET_XOVER_FILTER_UPDATE",            0},  //32-bit data,                  Q0
    {"DSM_API_SETGET_XOVER_SPKER_GAIN_PERCENTAGE",            27},  //32-bit data,                  Q0
    {"DSM_API_SETGET_PPR_KROSSOVER_FREQ",            9},  //32-bit data,                  Q0
    {"DSM_API_SETGET_INTERN_DEBUG1",            0},  //32-bit data,                  Q0
    {"DSM_API_SETGET_INTERN_DEBUG2",            28},  //32-bit data,                  Q0
};

#define PARAM_LEFT_MASK     (0x1000000)
#define ctrl_device "/dev/dsm_ctrl_dev"

struct dsm_params {
	uint32_t mode;
    uint32_t pcount;
    int32_t  pdata[256];
};

int main(int argc, char *argv[])
{
	char *end;
	int fd = -1, flags = 0, check = 2;
	int set = 0, get = 0, left = 0, right = 0;
    struct dsm_params params;

	/* handle (optional) flags first */
	while (1+flags < argc && argv[1+flags][0] == '-') {
		switch (argv[1+flags][1]) {
		case 'w': set = 1; break;
		case 'r': get = 1; break;
        case 'a': set = 1; left = 1; break;
		case 'b': set = 1; right = 1; break;
		default:
			fprintf(stderr, "Error: Unsupported option "
				"\"%s\"!\n", argv[1+flags]);
			help();
			exit(1);
		}

		flags++;
	}

	/*set option need one more args for value*/
	if (set)
		check++;

	if (argc < flags + check)
		help();

    fd = open(ctrl_device, O_RDWR);
    if (fd < 0){
        printf("dsm control device is not found\n");
        goto out;
    }

	if (set) {
		int pid = 0, fixed_val = 0;
        float value;

		pid = strtoul(argv[flags+1], &end, 0);
		value = strtof(argv[flags+2], &end);

        printf("pid %d, value %f\n", pid, value);
        if (pid < DSM_API_SETGET_PARAMS_COUNT &&
            pid > 0)
        {
            fixed_val = TO_FIX(value, pInfos[pid].qVal);
            if(left){
                params.pdata[0] = DSM_SET_LEFT_PARAM(pid);
                params.pdata[1] = fixed_val;
            }else if(right){
                params.pdata[0] = DSM_SET_RIGHT_PARAM(pid);
                params.pdata[1] = fixed_val;
            }else{
                params.pdata[0] = DSM_SET_LEFT_PARAM(pid);
                params.pdata[1] = fixed_val;
            }

            params.pcount = 1;
            write(fd, &params, sizeof(params));
		    fprintf(stderr, "Set :%03d  %25s with value %f 0x%08x\n", pid, pInfos[pid].name, value, fixed_val);
        }
	}
	else if (get) {
		int pid, vall;
        int i = 0, qVal = 0, *pdata;
        read(fd, &params, sizeof(params));

        if (params.pcount > 0  &&
            params.pcount < DSM_API_SETGET_PARAMS_COUNT){

            pdata = params.pdata;
            for (i = 0; i < params.pcount; i++){
                pid = *pdata++;
                vall = *pdata++;
                /* valr = *pdata++; */

                qVal = pInfos[pid].qVal;
				/* fprintf(stderr, " %03d:  %30s [%13f, %13f]\n", */
					/* pid, */
					/* pInfos[pid].name, */
					/* TO_DBL(vall, qVal), */
					/* TO_DBL(valr, qVal)); */
 				fprintf(stderr, " %03d:  %30s [%13f]\n",
					pid,
					pInfos[pid].name,
					TO_DBL(vall, qVal)
					);
            }
        }
	}

    close(fd);

out:
	exit(0);
}

