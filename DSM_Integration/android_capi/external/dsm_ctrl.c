
/*DSM set/get parameter main function*/

#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <fcntl.h>


static void help(void) __attribute__ ((noreturn));

static void help(void)
{
    fprintf(stderr,
        "Usage: dsm_ctrl [-r] [-w] [-a] [-b] ID [VALUE] \n"
        "  ID is the parameter ID number \n"
        "  VALUE is an integer value for write mode \n"
        "  OPTION is one of:\n"
        "    w (set parameter data)\n"
        "    r (read parameter data)\n"
        "    a (set parameter of left channel for stereo)\n"
        "    b (set parameter of right channel for stereo)\n");
    exit(1);
}

#ifndef TO_FIX
#define TO_FIX(a, q)    ((int)((a) * ((unsigned int)1<<(q))))
#endif

#ifndef TO_DBL
#define TO_DBL(a, q)    (double)((double)((double)(a)/(double)((unsigned int)1<<(q))))
#endif

#define DSM_API_SETGET_PARAMS_COUNT     (150)


typedef enum {
    DSM_API_MONO_SPKER                  = 0x00000000,//the mono speaker
    DSM_API_STEREO_SPKER                = 0x03000000,//the stereo speakers

    DSM_API_L_CHAN                      = 0x01000000,//the left channel speaker Id
    DSM_API_R_CHAN                      = 0x02000000,//the left channel speaker Id

    DSM_API_CHANNEL_1                   = 0x01000000,
    DSM_API_CHANNEL_2                   = 0x02000000,
    DSM_API_CHANNEL_3                   = 0x04000000,
    DSM_API_CHANNEL_4                   = 0x08000000,
    DSM_API_CHANNEL_5                   = 0x10000000,
    DSM_API_CHANNEL_6                   = 0x20000000,
    DSM_API_CHANNEL_7                   = 0x40000000,
    DSM_API_CHANNEL_8                   = 0x80000000,

    DSM_MAX_SUPPORTED_CHANNELS          = 8
} DSM_API_CHANNEL_ID;


#define DSM_SET_MONO_PARAM(cmdId)       ((cmdId&0x00FFFFFF)|DSM_API_MONO_SPKER)
#define DSM_SET_STEREO_PARAM(cmdId)     ((cmdId&0x00FFFFFF)|DSM_API_STEREO_SPKER)
#define DSM_SET_LEFT_PARAM(cmdId)       ((cmdId&0x00FFFFFF)|DSM_API_L_CHAN)
#define DSM_SET_RIGHT_PARAM(cmdId)      ((cmdId&0x00FFFFFF)|DSM_API_R_CHAN)

struct param_info {
    char name[80];
    uint32_t qVal;
};


static struct param_info pInfos[] = {
    //The following messages are read/write
    {"DSM_API_GET_MAXIMUM_CMD_ID",                  0},     //0, 32-bits data,                 Q0
    {"DSM_API_SETGET_ENABLE",                       0},     //1, 32-bits data,                 Q0
    {"DSM_API_SETGET_COILTEMP_THRESHOLD",          19},     //2, 32-bits data, in C degrees,   Q19
    {"DSM_API_SETGET_XCL_THRESHOLD",               27},     //3, 32-bits data, in mm,          Q27
    {"DSM_API_SETGET_LIMITERS_RELTIME",            30},     //4, 32-bits data, in seconds,     Q30
    {"DSM_API_SETGET_MAKEUP_GAIN",                 29},     //5, 32-bits data, in %,           Q29
    {"DSM_API_SETGET_RDC_AT_ROOMTEMP",             27},     //6, 32-bits data,                 Q27
    {"DSM_API_SETGET_COPPER_CONSTANT",             30},     //7, 32-bits data,                 Q30
    {"DSM_API_SETGET_COLDTEMP",                    19},     //8, 32-bits data,                 Q19
    {"DSM_API_SETGET_PITONE_GAIN",                 31},     //9, 32-bits data, in %,           Q31
    {"DSM_API_SETGET_LEAD_RESISTANCE",             27},     //10, 32-bits data,                 Q27
    {"DSM_API_SETGET_HPCUTOFF_FREQ",                9},     //11, 32-bits data, in Hz,          Q9
    {"DSM_API_SETGET_LFX_GAIN",                    30},     //12, 32-bits data,                 Q30

    //for impedence model updating
    {"DSM_API_SETGET_REF_FC",                       9},     //13, 32-bits data, in Hz,          Q9
    {"DSM_API_SETGET_REF_Q",                       29},     //14, 32-bits data,                 Q29
    {"DSM_API_INIT_F_Q_FILTERS",                    0},     //15, 32-bits data,                 Q0

    //The following messages are read only
    {"DSM_API_GET_ADAPTIVE_FC",                     9},     //16, 32-bits data, in Hz,          Q9
    {"DSM_API_GET_ADAPTIVE_Q",                     29},     //17, 32-bits data,                 Q29
    {"DSM_API_GET_ADAPTIVE_DC_RES",                27},     //18, 32-bits data,                 Q27
    {"DSM_API_GET_ADAPTIVE_COILTEMP",              19},     //19, 32-bits data,                 Q19
    {"DSM_API_GET_EXCURSION",                      27},     //20, 32-bits data,                 Q27

    //The fllowing 4 commands are used the buffers used for the following commands must be allocated
    //by the caller function. The maxim buffer size is 4K bytes.
    {"DSM_API_GET_PCM_INPUT_DATA",                  0},     //21, the buffer will be filled with 16-bit input PCM data to DSM module.
    {"DSM_API_GET_IV_DATA",                         0},     //22, the buffer will be filled with I and V data.
    {"DSM_API_GET_PCM_AND_IV_DATA",                 0},     //23, the buffer will be filled with PCM, I and V data. All data are in 16-bit integer.
    {"DSM_API_GET_PCM_OUTPUT_DATA",                 0},     //24, the buffer will be filled with 16-bit output PCM data of DSM module.

    {"DSM_API_SETGET_INTERN_DEBUG",                 0},     //25, This command is obsolete.
    {"DSM_API_SETGET_VLIMIT",                      27},     //26, 32-bits data,                 Q27
    {"DSM_API_SETGET_NEW_CONTROLS",                 0},     //27, This command is obsolete.

    {"DSM_API_SETGET_PILOT_ENABLE",                 0},     //28, 32-bits data,                 Q0
    {"DSM_API_SETGET_CLIP_ENABLE",                  0},     //29, 32-bits data,                 Q0
    {"DSM_API_SETGET_EXC_ENABLE",                   0},     //30, 32-bits data,                 Q0
    {"DSM_API_SETGET_THERMAL_ENABLE",               0},     //31, 32-bits data,                 Q0

    {"DSM_API_SETGET_EQ_BAND_FC",                   9},   //32, 32-bits data, in Hz,          Q9
    {"DSM_API_SETGET_EQ_BAND_Q",                   27},   //33, 32-bits data,                 Q27
    {"DSM_API_SETGET_EQ_BAND_ATTENUATION_DB",      20},   //34, 32-bits data, in db,          Q20
    {"DSM_API_SETGET_TARGET_EQ_BAND_ID",            0},   //35, 32-bits data,                 Q0
    {"DSM_API_SETGET_EQ_BAND_ENABLE",               0},   //36, 32-bits data,                 Q0. Bit field defintions: bit0:band1,bit1:band2,bit2:band3,bit3:band4
    {"DSM_API_GET_EQ_BAND_COEFFICIENTS",            0},   //37, This command is obsolete.

    // ZIMP - imdepdence filter coefficients, for power from voltage
    {"DSM_API_GET_ZIMP_A1",                        28},   //38, 32-bits data,                 Q28
    {"DSM_API_GET_ZIMP_A2",                        28},   //39, 32-bits data,                 Q28
    {"DSM_API_GET_ZIMP_B0",                        28},   //40, 32-bits data,                 Q28
    {"DSM_API_GET_ZIMP_B1",                        28},   //41, 32-bits data,                 Q28
    {"DSM_API_GET_ZIMP_B2",                        28},   //42, 32-bits data,                 Q28

    // EQ Biquad coefficients
    {"DSM_API_SETGET_EQ1_A1",                      28},   //43, 32-bits data,                 Q28
    {"DSM_API_SETGET_EQ1_A2",                      28},   //44, 32-bits data,                 Q28
    {"DSM_API_SETGET_EQ1_B0",                      28},   //45, 32-bits data,                 Q28
    {"DSM_API_SETGET_EQ1_B1",                      28},   //46, 32-bits data,                 Q28
    {"DSM_API_SETGET_EQ1_B2",                      28},   //47, 32-bits data,                 Q28

    {"DSM_API_SETGET_FC_GUARD_BAND",                      9},   //48, 32-bits data,                 Q9
    {"DSM_API_SETGET_FC_NARROW_RANGE",                    9},   //49, 32-bits data,                 Q9
    {"DSM_API_SETGET_FC_WIDE_RANGE",                      9},   //50, 32-bits data,                 Q9
    {"DSM_API_SETGET_PPR_XOVER_FREQ_Hz",                  9},   //51, 32-bits data,                 Q9
    {"DSM_API_SETGET_ENABLE_PPR",                         0},   //52, 32-bits data,                 Q0

    {"DSM_API_SETGET_RDC_SCALING",                 25},   //53, 32-bits data,                 Q25
    {"DSM_API_SETGET_MBDRC_TARGET_SUBBAND_ID",      0},   //54, 32-bits data,                 Q0
    {"DSM_API_SETGET_MBDRC_ENABLE",                 0},   //55, 32-bits data,                 Q0
    {"DSM_API_SETGET_DRC_TRHESHOLD",               20},   //56, 32-bits data,                 Q20
    {"DSM_API_SETGET_DRC_RATIO",                   20},   //57, 32-bits data,                 Q20

    {"DSM_API_SETGET_DRC_ATTACK",                  20},   //58, 32-bits data,                 Q20
    {"DSM_API_SETGET_DRC_RELEASE",                 20},   //59, 32-bits data,                 Q20
    {"DSM_API_SETGET_MBDRC_CUTOFF_F1",              0},   //60, 32-bits data,                 Q0
    {"DSM_API_SETGET_MBDRC_CUTOFF_F2",              0},   //61, 32-bits data,                 Q0
    {"DSM_API_SETGET_MBDRC_DEBUG",                  0},   //62, 32-bits data,                 Q0

    {"DSM_API_SETGET_TRAJECTORY",                   0},   //63, 32-bits data,                 Q0
    {"DSM_API_SETGET_GUARD_BEEN_MEAN_SCALE",        30},  //64, 32-bits data,                 Q30

    {"DSM_API_SET_UPDATE_DELAY",                    0},   //65, 32-bits data,                 Q0
    {"DSM_API_SETGET_DELAY",                        0},   //66, 32-bits data,                 Q0
    {"DSM_API_GET_MAXIMUM_DELAY",                   0},   //67, 32-bits data,                 Q0

    {"DSM_API_SETGET_ENABLE_FAST_FC",               0},   //68, 32-bits data,                 Q0

    {"DSM_API_SETGET_ORM_QUALIFY_THRESH",           30},  //69, 32-bits data,                 Q30

    {"DSM_API_SETGET_ENABLE_LOGGING",               0},   //70, 32-bits data,                 Q0

    {"DSM_API_SETGET_SPEAKER_PARAM_LFX_A1",        28},   //71, 32-bits data,                 Q28
    {"DSM_API_SETGET_SPEAKER_PARAM_LFX_A2",        28},   //72, 32-bits data,                 Q28
    {"DSM_API_SETGET_SPEAKER_PARAM_LFX_B0",        28},   //73, 32-bits data,                 Q28
    {"DSM_API_SETGET_SPEAKER_PARAM_LFX_B1",        28},   //74, 32-bits data,                 Q28
    {"DSM_API_SETGET_SPEAKER_PARAM_LFX_B2",        28},   //75, 32-bits data,                 Q28
    {"DSM_API_SETGET_SPEAKER_PARAM_TCTH1",         20},   //76, 32-bits data,                 Q20
    {"DSM_API_SETGET_SPEAKER_PARAM_TCTH2",         20},   //77, 32-bits data,                 Q20
    {"DSM_API_SETGET_SPEAKER_PARAM_RTH1",          22},   //78, 32-bits data,                 Q22
    {"DSM_API_SETGET_SPEAKER_PARAM_RTH2",          22},   //79, 32-bits data,                 Q22
    {"DSM_API_SETGET_SPEAKER_PARAM_ADMIT_A1",      28},   //80, 32-bits data,                 Q28
    {"DSM_API_SETGET_SPEAKER_PARAM_ADMIT_A2",      28},   //81, 32-bits data,                 Q28
    {"DSM_API_SETGET_SPEAKER_PARAM_ADMIT_B0",      28},   //82, 32-bits data,                 Q28
    {"DSM_API_SETGET_SPEAKER_PARAM_ADMIT_B1",      28},   //83, 32-bits data,                 Q28
    {"DSM_API_SETGET_SPEAKER_PARAM_ADMIT_B2",      28},   //84, 32-bits data,                 Q28

    {"DSM_API_SETGET_SPEAKER_PARAM_UPDATE",         0},   //85, unsigned 32-bit,              Q0

    {"DSM_API_SETGET_EQ_FILTER_TYPE",				0},   //86, unsigned 32-bit,              Q0

    {"DSM_API_SETGET_DEB_NOTCH_FC",                 9},   //87, 32-bit,                       Q0

    // special APIs
    {"DSM_API_GET_FIRMWARE_BUILD_TIME",             0},   //88, C string
    {"DSM_API_GET_FIRMWARE_BUILD_DATE",             0},   //89, C string
    {"DSM_API_GET_FIRMWARE_VERSION",                0},   //90, C string
    {"DSM_API_GET_CHIPSET_MODEL",                   0},   //91, C string
    {"DSM_API_GET_ENDIAN",                          0},   //92, 32-bits data = 0xdeadbeef,    Q0

    {"DSM_API_SETGET_LFX_Q_SLOPE",                 29},   //93, 32-bits data,                 Q29

    {"DSM_API_SETGET_VIRTUAL_V_ENABLE",             0},   //94, 32-bits data,                 Q0
    {"DSM_API_SETGET_FORCED_VIRTUAL_V",             0},   //95, 32-bits data,                 Q0
    {"DSM_API_SETGET_I_SHIFT_BITS",                 0},   //96, 32-bits data,                 Q0

    {"DSM_API_SETGET_EXC_FUNC_GAIN_ADJUSTED",       0},   //97, 32-bits data,                 Q0

    {"DSM_API_SETGET_FADE_IN_TIME_MS",              0},   //98, 32-bits data,                 Q0
    {"DSM_API_SETGET_FADE_OUT_TIME_MS",             0},   //99, 32-bits data,                 Q0

    //This command takes effect only after
    //DSM_API_FADE_OUT_TIME_MS
    {"DSM_API_SETGET_FADE_OUT_MUTE_TIME_MS",        0},  //100, 32-bits data,                 Q0

    {"DSM_API_SETGET_FADE_IN_ENABLE",               0},  //101, 32-bits data,                 Q0
    {"DSM_API_SETGET_FADE_OUT_ENABLE",              0},  //102, 32-bits data,                 Q0

    {"DSM_API_SETGET_ENABLE_MULTICHAN_LINKING",     0},  //103, 32-bit data,                  Q0
    {"DSM_API_SETGET_ENABLE_SMART_PT",              0},  //104, 32-bit data,                  Q0
    {"DSM_API_SETGET_PILOTTONE_SILENCE_THRESHOLD",  0},  //105, 32-bit data,                  Q0
    {"DSM_API_SETGET_SILENCE_PILOTTONE_GAIN_Q31",   31}, //106, 32-bit data,                  Q31
    {"DSM_API_SETGET_SILENCE_FRAMES",				0},  //107, 32-bit data,						   Q0
    {"DSM_API_SETGET_PILOTTONE_TRANSITION_FRAMES",            0},  //108, 32-bit data,                  Q0
    {"DSM_API_SETGET_PILOTTONE_SMALL_SIGNAL_THRESHOLD",       0},  //109, 32-bit data,                  Q0
    {"DSM_API_SETGET_SMALL_SIGNAL_PILOTTONE_GAIN_Q31",       31},  //110, 32-bit data,                  Q0
    {"DSM_API_SETGET_ENABLE_LINKWITZ_EQ",            0},	//111, 32-bit data,                  Q0
    {"DSM_API_SETGET_CHANNEL_MASK",            0},  //112, 32-bit data,                  Q0
    {"DSM_API_SETGET_ENABLE_FF_FB_MODULES",            0},  //113, 32-bit data,                  Q0
    {"DSM_API_SETGET_ENABLE_CROSSOVER",            0},  //114, 32-bit data,                  Q0
    {"DSM_API_SETGET_AUX_EQ_BAND_FC",            9},  //115, 32-bit data,                  Q0
    {"DSM_API_SETGET_AUX_EQ_BAND_Q",            29},  //116, 32-bit data,                  Q0
    {"DSM_API_SETGET_AUX_EQ_BAND_ATTENUATION_DB",            20},  //117, 32-bit data,                  Q0
    {"DSM_API_SET_EQ_AUX_BAND_COEFF_UPDATE",            0},  //118, 32-bit data,                  Q0
    {"DSM_API_SETGET_AUX_EQ_BAND_ENABLE",            0},  //119, 32-bit data,                  Q0
    {"DSM_API_SETGET_ENABLE_AUX_CROSSOVER",            0},  //120, 32-bit data,                  Q0
    {"DSM_API_SETGET_ENABLE_AUX1_DELAYED_SAMPLES",            0},  //121, 32-bit data,                  Q0
    {"DSM_API_SETGET_ENABLE_AUX2_DELAYED_SAMPLES",            0},  //122, 32-bit data,                  Q0
    {"DSM_API_SETGET_ENABLE_AUX3_DELAYED_SAMPLES",            0},  //123, 32-bit data,                  Q0
    {"DSM_API_SETGET_ENABLE_AUX4_DELAYED_SAMPLES",            0},  //124, 32-bit data,                  Q0
    {"DSM_API_SETGET_SPEECH_GUARD_BINS",            0},  //125, 32-bit data,                  Q0
    {"DSM_API_SETGET_MEAN_SPEECH_THRESHOLD",            31},  //126, 32-bit data,                  Q31
    {"DSM_API_SETGET_HPCUTOFF_FREQ_AUX",            9},  //127, 32-bit data,                  Q9
    {"DSM_API_SETGET_AUX_HP_FILTER_ENABLE",            0},  //128, 32-bit data,                  Q0
    {"DSM_API_SETGET_STEREO_CROSSOVER_MODE",            0},  //129, 32-bit data,                  Q0
    {"DSM_API_SETGET_ENABLE_UPDATE_FC_Q",            0},  //130, 32-bit data,                  Q0
    {"DSM_API_SETGET_RECEIVER_PHYSICAL_LAYOUT",            0},  //131, 32-bit data,                  Q0
    {"DSM_API_SETGET_MAX_V_SAMPLE",            0},  //132, 32-bit data,                  Q0
    {"DSM_API_SETGET_DEB_NOTCH_Q",            28},  //133, 32-bit data,                  Q0
    {"DSM_API_SETGET_DEB_ID",                  0},  //134, 32-bit data,                  Q0
    {"DSM_API_SETGET_FB_SKIPPING_MS",            0},  //135, 32-bit data,                  Q0
    {"DSM_API_GET_ADAPTIVE_PARAMS",            0},  //136, 32-bit data,                  Q0
    {"DSM_API_GET_ADAPTIVE_STATISTICS",            0},  //137, 32-bit data,                  Q0
    {"DSM_API_SETGET_SILENCE_UNMUTED_IN_SPT",            0},  //138, 32-bit data,                  Q0
    {"DSM_API_SETGET_Q_ADJUSTMENT",            29},  //139, 32-bit data,                  Q29
    {"DSM_API_SETGET_XOVER_MIXING_ENABLED",            0},  //140, 32-bit data,                  Q0
    {"DSM_API_SETGET_XOVER_BOOST_GAIN_PERCENTAGE",            27},  //141, 32-bit data,                  Q0
    {"DSM_API_SETGET_XOVER_FILTER_CUTOFF_FC",            9},  //142, 32-bit data,                  Q0
    {"DSM_API_SETGET_XOVER_FILTER_Q",           29},  //143, 32-bit data,                  Q29
    {"DSM_API_SET_XOVER_FILTER_UPDATE",            0},  //144, 32-bit data,                  Q0
    {"DSM_API_SETGET_XOVER_SPKER_GAIN_PERCENTAGE",            27},  //145, 32-bit data,                  Q27
    {"DSM_API_SETGET_PPR_KROSSOVER_FREQ",            9},  //146, 32-bit data,                  Q9
    {"DSM_API_SETGET_INTERN_DEBUG1",            0},  //147, 32-bit data,                  Q0
    {"DSM_API_SETGET_INTERN_DEBUG2",            28},  //148, 32-bit data,                  Q28
};

#define PARAM_LEFT_MASK     (0x1000000)
#define ctrl_device "/dev/dsm_ctrl_dev"

struct dsm_params {
    uint32_t opcode;
    uint32_t pcount;
    uint32_t pdata[254];
};

int main(int argc, char *argv[])
{
    char *end;
    int fd = -1, flags = 0, check = 2;
    int set = 0, get = 0, left = 0, right = 0, qmod = 0;
    struct dsm_params params;

    /* handle (optional) flags first */
    while (1+flags < argc && argv[1+flags][0] == '-') {
        switch (argv[1+flags][1]) {
        case 'w': set = 1; break;
        case 'r': get = 1; break;
        case 'm': set = 1; qmod = 1; break;
        case 'a': set = 1; left = 1; break;
        case 'b': set = 1; right = 1; break;
        default:
            fprintf(stderr, "Error: Unsupported option "
                     "\"%s\"!\n", argv[1+flags]);
            help();
            exit(1);
        }

        flags++;
    }

    /*set option need one more args for value*/
    if (set)
      check++;

    if (argc < flags + check)
        help();

    fd = open(ctrl_device, O_RDWR);
    if (fd < 0){
        printf("dsm control device is not found\n");
        goto out;
    }

    if (set) {
        int pid = 0, fixed_val = 0;
        float value;

        pid = strtoul(argv[flags+1], &end, 0);
        if (qmod == 0){
			value = strtof(argv[flags+2], &end);

			printf("pid %d, value %f\n", pid, value);
			if (pid < DSM_API_SETGET_PARAMS_COUNT &&
					pid > 0)
			{
				fixed_val = TO_FIX(value, pInfos[pid].qVal);
				if(left){
					params.pdata[0] = DSM_SET_LEFT_PARAM(pid);
					params.pdata[1] = fixed_val;
				}else if(right){
					params.pdata[0] = DSM_SET_RIGHT_PARAM(pid);
					params.pdata[1] = fixed_val;
				}else{
					/* params.pdata[0] = DSM_SET_RIGHT_PARAM(pid); */
					params.pdata[0] = DSM_SET_STEREO_PARAM(pid);
					params.pdata[1] = fixed_val;
				}

				params.opcode = 0xABEFCDAB;
				params.pcount = 1;
				write(fd, &params, sizeof(params));
				fprintf(stderr, "Set :%03d  %25s with value %f 0x%08x\n", pid, pInfos[pid].name, value, fixed_val);
			}
		} else {
			params.opcode = 0xAABBCCDD;
			params.pcount = 0;
			params.pdata[0]  = pid;
			write(fd, &params, sizeof(params));
			fprintf(stderr, "Set query mode to %d\n", pid);
		}
    }
    else if (get) {
        int pid, vall, valr;
        int i = 0, qVal = 0, *pdata;
        do {
			read(fd, &params, sizeof(params));

			if (params.pcount > 0  &&
					params.pcount < DSM_API_SETGET_PARAMS_COUNT){

				pdata = params.pdata;
				for (i = 0; i < params.pcount; i++){
					pid = *pdata++;
					vall = *pdata++;
					valr = *pdata++;
					/* valr = vall; */

					qVal = pInfos[pid].qVal;
					fprintf(stderr, " %03d: Q: %03d  %30s [%13f   %13f]\n",
							pid,
							qVal,
							pInfos[pid].name,
							TO_DBL(vall, qVal),
							TO_DBL(valr, qVal));
				}
			}
		} while (params.opcode == 0);

		fprintf(stderr, "Build Data: %s Build Time: %s\n", (char *)(pdata+1), (char *)(pdata+10));
    }

    close(fd);

out:
    exit(0);
}
