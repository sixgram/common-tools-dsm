/*========================================================================
   This file contains DSM AFE implementation
 ====================================================================== */
#ifdef ENABLE_DSM
/*==========================================================================
  Include files
  ========================================================================== */
#include "AFEPortManagement.h"
#include "dsm_api.h"

#define DSM_MOD_MAGIC       (0x1000DEAD)
/*==========================================================================
Global Variables
 ========================================================================== */

static dsm_shared_handle_t shared_handle;

/*==========================================================================
  Function Declarations
========================================================================== */
static ADSPResult afe_bg_proc_dsm_create(afe_background_proc_t **bg_proc_ptr_ptr, afe_dev_port_t *dev_port,
         uint16_t alg_blk_len,void *lib_obj);
static void afe_bg_proc_dsm_destroy(afe_background_proc_t *bg_proc_ptr);
static void dsm_process_fn_rx (void *lib_obj, int8_t* in_buffer, int16 frame_size, int16 channel_spacing);
static void dsm_process_fn_tx (void *lib_obj, int8_t* in_buffer, int16 frame_size, int16 channel_spacing);
static void dsm_output_fn_rx (void *lib_obj, int8_t* out_buffer, int16 frame_size, int16 channel_spacing);
static void dsm_output_fn_tx (void *lib_obj, int8_t* out_buffer, int16 frame_size, int16 channel_spacing);
ADSPResult afe_port_deinit_dsm(afe_dev_port_t *dev_port);

afe_dsm_module_t *afe_port_get_dsm_module(afe_dev_port_t *dev_port)
{
    afe_dsm_module_t *afe_dsm_ptr;

    afe_dsm_ptr = (RX_DIR == dev_port->dir)?(shared_handle.rx_module):(shared_handle.tx_module);
    if (afe_dsm_ptr && (DSM_MOD_MAGIC == afe_dsm_ptr->magic) &&
        (dev_port == afe_dsm_ptr->dev_port))
    {
        return afe_dsm_ptr;
    }
    else
    {
        return NULL;
    }
}

ADSPResult afe_port_alloc_resources_for_module(afe_dev_port_t *pDevPort, uint32_t mod_id)
{
    ADSPResult result = ADSP_EOK;
    uint16_t vote_now = FALSE;

    if(AFE_PORT_STATE_RUN == pDevPort->port_state)
    {
        vote_now = TRUE;
    }

    switch(mod_id)
    {
        case AFE_MODULE_DSM_RX:
        {
            result = afe_mmpm_voting(pDevPort, AFE_MMPM_DSM_FF_TOPOLOGY, AFE_MMPM_VOTE, vote_now);
            if (ADSP_FAILED(result))
            {
                MSG_1(MSG_SSID_QDSP6, DBG_ERROR_PRIO, "MMPM voting failed while enabling DSM module: 0x%x", pDevPort->intf);
                return result;
            }
            break;
        }
        case AFE_MODULE_DSM_TX:
        {
            result = afe_mmpm_voting(pDevPort, AFE_MMPM_DSM_FB_TOPOLOGY, AFE_MMPM_VOTE, vote_now);
            if (ADSP_FAILED(result))
            {
                MSG_1(MSG_SSID_QDSP6, DBG_ERROR_PRIO, "MMPM voting failed while enabling DSM module: 0x%x", pDevPort->intf);
                return result;
            }
            break;
        }
        default:
            result = ADSP_EUNSUPPORTED;
            break;
    }

    return result;
}

void afe_port_dealloc_resources_for_module(afe_dev_port_t *pDevPort, uint32_t mod_id)
{
    uint16_t vote_now = FALSE;

    if(AFE_PORT_STATE_RUN == pDevPort->port_state)
    {
        vote_now = TRUE;
    }

    switch(mod_id)
    {
    case AFE_MODULE_DSM_RX:
        afe_mmpm_voting(pDevPort, AFE_MMPM_DSM_FF_TOPOLOGY, AFE_MMPM_DEVOTE, vote_now);
        break;

    case AFE_MODULE_DSM_TX:
        afe_mmpm_voting(pDevPort, AFE_MMPM_DSM_FB_TOPOLOGY, AFE_MMPM_DEVOTE, vote_now);
        break;

    default:
        break;
    }
}


ADSPResult afe_port_alloc_memory_for_module(afe_dev_port_t *pDevPort, uint32_t mod_id)
{
    ADSPResult result = ADSP_EOK;

    switch(mod_id)
    {
    case AFE_MODULE_DSM_RX:
        if (NULL == shared_handle.rx_module)
        {
            afe_dsm_module_t *afe_dsm_ptr = NULL;
            if (NULL == (afe_dsm_ptr = (afe_dsm_module_t*)qurt_elite_memory_malloc((sizeof(afe_dsm_module_t)), QURT_ELITE_HEAP_DEFAULT)))
            {
                MSG(MSG_SSID_QDSP6, DBG_ERROR_PRIO, "Failed to create DSM struct object");
                return ADSP_ENOMEMORY;
            }
            memset(afe_dsm_ptr, 0, sizeof(afe_dsm_module_t));
            afe_dsm_ptr->magic = DSM_MOD_MAGIC;
            afe_dsm_ptr->dev_port = pDevPort;
            qurt_elite_mutex_init(&afe_dsm_ptr->dsm_port_mutex);
            shared_handle.rx_module = afe_dsm_ptr;
        }
        break;

    case AFE_MODULE_DSM_TX:
        if (NULL == shared_handle.tx_module)
        {
            afe_dsm_module_t *afe_dsm_ptr = NULL;
            if (NULL == (afe_dsm_ptr = (afe_dsm_module_t*)qurt_elite_memory_malloc((sizeof(afe_dsm_module_t)), QURT_ELITE_HEAP_DEFAULT)))
            {
                MSG(MSG_SSID_QDSP6, DBG_ERROR_PRIO, "Failed to create DSM struct object");
                return ADSP_ENOMEMORY;
            }
            memset(afe_dsm_ptr, 0, sizeof(afe_dsm_module_t));
            afe_dsm_ptr->magic = DSM_MOD_MAGIC;
            afe_dsm_ptr->dev_port = pDevPort;
            qurt_elite_mutex_init(&afe_dsm_ptr->dsm_port_mutex);
            shared_handle.tx_module = afe_dsm_ptr;
        }
        break;

    default:
        return ADSP_EBADPARAM;
        break;
    }
    return result;
}

void afe_port_dealloc_memory_for_module(afe_dev_port_t *pDevPort, uint32_t mod_id)
{
    afe_dsm_module_t *afe_dsm_ptr;

    switch(mod_id)
    {
    case AFE_MODULE_DSM_RX:
        afe_dsm_ptr = shared_handle.rx_module;
        if(NULL != afe_dsm_ptr && pDevPort == afe_dsm_ptr->dev_port)
        {
            shared_handle.rx_module = NULL;
            afe_dsm_ptr->magic = 0;
            qurt_elite_mutex_destroy(&afe_dsm_ptr->dsm_port_mutex);
            qurt_elite_memory_free(afe_dsm_ptr);
        }
        break;

    case AFE_MODULE_DSM_TX:
        afe_dsm_ptr = shared_handle.tx_module;
        if(NULL != afe_dsm_ptr && pDevPort == afe_dsm_ptr->dev_port)
        {
            shared_handle.tx_module = NULL;
            afe_dsm_ptr->magic = 0;
            qurt_elite_mutex_destroy(&afe_dsm_ptr->dsm_port_mutex);
            qurt_elite_memory_free(afe_dsm_ptr);
        }
        break;

    default:
        break;
    }
}

/*==========================================================================
  Function definies
========================================================================== */
/**
 * DSM
 *
 * setting config & enable is possible only in CONFIG state
 * setting disable is possible only in STOP state.
 */

ADSPResult dsm_enable_without_apr(afe_dev_port_t *pDevPort)
{
    afe_dsm_module_t *afe_dsm_ptr;
    ADSPResult result = ADSP_EOK;

    // cannot disable in non-stop state, and cannot enable in run state.
    if (((AFE_PORT_STATE_CONFIG != pDevPort->port_state) && (AFE_PORT_STATE_STOP != pDevPort->port_state)))
    {
        MSG_2(MSG_SSID_QDSP6, DBG_ERROR_PRIO, "DSM enable failed, Accept in CONFIG/STOP state only, (state, id): %d, %d",
            pDevPort->port_state, pDevPort->intf);
        return ADSP_EBADPARAM;
    }

    afe_dsm_ptr = afe_port_get_dsm_module(pDevPort);
    if (NULL == afe_dsm_ptr && TX_DIR == pDevPort->dir)
    {
        //MSG(MSG_SSID_QDSP6, DBG_HIGH_PRIO, "[MAXIM]: Set params called from AFE_PARAM_ID_ENABLE");
        if (ADSP_EOK == (result = afe_port_alloc_memory_for_module(pDevPort, AFE_MODULE_DSM_TX)))
        {
            afe_dsm_ptr = afe_port_get_dsm_module(pDevPort);

            if (ADSP_EOK == (result = afe_port_alloc_resources_for_module(pDevPort, AFE_MODULE_DSM_TX)))
            {
                afe_dsm_ptr->memory_allocated = TRUE; //enabled but not active yet, since init is not done.
            }
            else
            {
                MSG_1(MSG_SSID_QDSP6, DBG_HIGH_PRIO, "port %x: DSM: failed to allocate MMPM resources, disabling the module", pDevPort->intf);
                afe_port_dealloc_memory_for_module(pDevPort, AFE_MODULE_DSM_TX);
            }
        }
    }

    MSG_1(MSG_SSID_QDSP6, DBG_HIGH_PRIO, "port %x: DSM: enabled", pDevPort->intf);

    return result;
}

ADSPResult afe_port_set_dsm_module_params(afe_dev_port_t *pDevPort,uint32_t param_id,
                                                          int8_t *param_buffer_ptr, uint16_t param_size)
{
    ADSPResult result = ADSP_EOK;
    afe_dsm_module_t *afe_dsm_ptr;

    if (RX_DIR != pDevPort->dir ||                             //not rx port
        NULL == param_buffer_ptr)                         //param buffer is null
    {
        MSG_1(MSG_SSID_QDSP6, DBG_ERROR_PRIO, "Port %x: dsm: is not Rx or NULL pointer.",pDevPort->intf);
        return ADSP_EUNSUPPORTED;
    }

    //DSM can only be enabled in this port
    if (DSM_RX_PORT_ID != pDevPort->intf)
        return ADSP_EOK;

    switch(param_id) {
    case AFE_PARAM_ID_ENABLE:
    {
        if (param_size < sizeof(afe_mod_enable_param_t))
        {
            MSG_1(MSG_SSID_QDSP6, DBG_HIGH_PRIO, "port %x: DSM: improper payload size at ENABLE/DISABLE", pDevPort->intf);
                return ADSP_EBADPARAM;
        }
        else
        {
            int16_t enable_flag = *((int16_t*)param_buffer_ptr);

            // cannot disable in non-stop state, and cannot enable in run state.
            if ((TRUE == enable_flag) && ((AFE_PORT_STATE_CONFIG != pDevPort->port_state) &&
                (AFE_PORT_STATE_STOP != pDevPort->port_state)))
            {
                MSG_3(MSG_SSID_QDSP6, DBG_ERROR_PRIO, "DSM en/disable=%d failed, Accept in CONFIG/STOP state only, (state, id): %d, %d", \
                    enable_flag,pDevPort->port_state, pDevPort->intf);

                return ADSP_EBADPARAM;
            }

            afe_dsm_ptr = afe_port_get_dsm_module(pDevPort);

            if( (NULL == afe_dsm_ptr) && (FALSE == enable_flag) )
            {
                //this may happen because, we disable dsm when port is stopped. client may disable later.
                //nothing to do. just ack OK.
                MSG_1(MSG_SSID_QDSP6, DBG_HIGH_PRIO, "port %x: DSM: disabled by client.", pDevPort->intf);
                return ADSP_EOK;
            }

            if ((NULL == afe_dsm_ptr) && (TRUE == enable_flag)) //if enable, then alloc memory if necessary (done inside alloc_res) and set true flag
            {
                MSG(MSG_SSID_QDSP6, DBG_HIGH_PRIO, "DSM: Set params called from AFE_PARAM_ID_ENABLE");
                if (ADSP_EOK == (result = afe_port_alloc_memory_for_module(pDevPort, AFE_MODULE_DSM_RX)))
                {
                    afe_dsm_ptr = afe_port_get_dsm_module(pDevPort);

                    if (ADSP_EOK == (result = afe_port_alloc_resources_for_module(pDevPort, AFE_MODULE_DSM_RX)))
                    {
                        afe_dsm_ptr->memory_allocated = TRUE; //enabled but not active yet, since init is not done.
                        MSG(MSG_SSID_QDSP6, DBG_FATAL_PRIO, "DSM afe_dsm_ptr->memory_allocated= TRUE is set");
                    }
                    else
                    {
                        MSG_1(MSG_SSID_QDSP6, DBG_HIGH_PRIO, "port %x: DSM: failed to allocate MMPM resources, disabling the module", pDevPort->intf);
                        afe_port_dealloc_memory_for_module(pDevPort, AFE_MODULE_DSM_RX);
                    }
                }
            }

            MSG_2(MSG_SSID_QDSP6, DBG_HIGH_PRIO, "port %x: DSM enable/disable status = %d", pDevPort->intf, enable_flag);
        }

        break;
    }
    case AFE_PARAM_ID_DSM_CFG:
    {
  //      uint32_t mode = 0;
        uint32_t pcount = 0;

        afe_dsm_ptr = afe_port_get_dsm_module(pDevPort);
        if (NULL == afe_dsm_ptr)
        {
            MSG_1(MSG_SSID_QDSP6, DBG_HIGH_PRIO, "port %x: DSM is not enabled!", pDevPort->intf);
            return ADSP_EUNSUPPORTED;
        }

        if (param_size < sizeof(afe_mod_enable_param_t) )
        {
            MSG_1(MSG_SSID_QDSP6, DBG_HIGH_PRIO, "port %x: DSM: improper payload size at DSM_CFG", pDevPort->intf);
            return ADSP_EBADPARAM;
        }

 //       mode = *((uint32_t*)param_buffer_ptr);
        param_buffer_ptr += sizeof(uint32_t);
        pcount = *((uint32_t*)param_buffer_ptr);
        param_buffer_ptr += sizeof(uint32_t);

        /*If DSM is already running, set the parameters to algorithm directly*/
        if ((TRUE == afe_dsm_ptr->memory_allocated) &&
            (TRUE == afe_dsm_ptr->resources_allocated))
        {
            if (pcount > 0 && pcount < 128)
            {
                DSM_API_Set_Params(afe_dsm_ptr->dsm_handle->algo_lib,
                    pcount, (unsigned int*)param_buffer_ptr);
            }
        }
        /*Buffer parameters for DSM initialize*/
        else
        {
            if  (pcount > 0 && pcount < 128)
            {
                shared_handle.pcount        = pcount;

                memscpy(shared_handle.pdata, sizeof(shared_handle.pdata),
                    param_buffer_ptr,  sizeof(int32_t)*2*pcount);
            }
        }
    }
    break;

    default:
        result = ADSP_EUNSUPPORTED;
        MSG_1(MSG_SSID_QDSP6, DBG_HIGH_PRIO, "afe_port_set_dsm_module_params() :: param ID = 0X%x not supported \n", param_id);
    }

    return result;
}

static uint8_t getArray[] =
{
    DSM_API_SETGET_ENABLE                      ,    //32-bits data, Q0.                  valid floating-point range = {DSM_API_SMART_PA_DISABLE_MODE, DSM_API_SMART_PA_ENABLE_MODE, DSM_API_SMART_PA_BYPASS_ADD_MODE}
    DSM_API_SETGET_COILTEMP_THRESHOLD          ,    //32-bits data, Q19, in C degrees,   valid floating-point range = [0, 4096)
    DSM_API_SETGET_XCL_THRESHOLD               ,    //32-bits data, Q27, in millimeters, valid floating-point range = [0, 16)
    DSM_API_SETGET_LIMITERS_RELTIME            ,    //32-bits data, Q30, in seconds,     valid floating-point range = [0, 2)
    DSM_API_SETGET_MAKEUP_GAIN                 ,    //32-bits data, Q29, in %,           valid floating-point range = [0, 4)
    DSM_API_SETGET_RDC_AT_ROOMTEMP             ,    //32-bits data, Q27,                 valid floating-point range = [0, 16)
    DSM_API_SETGET_COPPER_CONSTANT             ,    //32-bits data, Q30,                 valid floating-point range = [0, 2)
    DSM_API_SETGET_COLDTEMP                    ,    //32-bits data, Q19,                 valid floating-point range = [0, 4096)
    DSM_API_SETGET_PITONE_GAIN                 ,    //32-bits data, Q31, in %,           valid floating-point range = [0, 1)
    DSM_API_SETGET_LEAD_RESISTANCE             ,   //32-bits data, Q27,                 valid floating-point range = [0, 16)
    DSM_API_SETGET_HPCUTOFF_FREQ               ,   //32-bits data, Q9, in Hz,           valid floating-point range = [0, 4194304)
    DSM_API_SETGET_LFX_GAIN                    ,   //32-bits data, Q30,                 valid floating-point range = (0, 1]

    //for impedance model updating
    DSM_API_SETGET_REF_FC                      ,   //32-bits data, Q9, in Hz,           valid floating-point range = [0, 4194304)
    DSM_API_SETGET_REF_Q                       ,   //32-bits data, Q29,                 valid floating-point range = [0, 4)
    DSM_API_INIT_F_Q_FILTERS                   ,   //32-bits data, Q0,                  valid floating-point range = {0, 1}

    //The following messages are read only
    DSM_API_GET_ADAPTIVE_FC                    ,   //32-bits data, Q9, in Hz,           valid floating-point range = [0, 4194304)
    DSM_API_GET_ADAPTIVE_Q                     ,   //32-bits data, Q29,                 valid floating-point range = [0, 4)
    DSM_API_GET_ADAPTIVE_DC_RES                ,   //32-bits data, Q27,                 valid floating-point range = [0, 16)
    DSM_API_GET_ADAPTIVE_COILTEMP              ,   //32-bits data, Q19,                 valid floating-point range = [0, 4096)
    DSM_API_GET_EXCURSION                      ,   //32-bits data, Q27,                 valid floating-point range = [0, 16)

    DSM_API_SETGET_VLIMIT                      ,   //32-bits data, Q27, in percentage.  valid floating-point range = [0, 16)
    DSM_API_SETGET_EQ_BAND_FC                  ,   //32-bits data, Q9,  in Hz,          valid floating-point range = [0, 4194304)
    DSM_API_SETGET_EQ_BAND_Q                   ,   //32-bits data, Q29,                 valid floating-point range = [0, 4)
    DSM_API_SETGET_EQ_BAND_ATTENUATION_DB      ,   //32-bits data, Q20, in db,          valid floating-point range = [-2048, 2048)
    DSM_API_SETGET_TARGET_EQ_BAND_ID           ,   //32-bits data, Q0,                  valid floating-point range = {1, 2, 3, ..., 32}
    DSM_API_SETGET_EQ_BAND_ENABLE              ,   //32-bits data, Q0.                  Bit field definitions: bit0:band1,...,bit31:band32, valid floating-point range = {0,...,0xFFFFFFFF}
    DSM_API_SETGET_MBDRC_TARGET_SUBBAND_ID     ,			//32-bit data, Q0, multi-band DRC channel ID. It is a control command to updates DRC parameters (Th, ratio, aTime,rTime,f1,f2).
                                                        //1 = sub-band1; 2 = sub-band2; 3 = sub-band3, valid floating-point range = {1, 2, 3}
	DSM_API_SETGET_MBDRC_ENABLE                ,   //32-bit data, Q0,  enable/disable a sub-band of DRC.
                                                        //Bit field definitions: bit0:DRC band1, bit1: DRC subband2, bit3:DRC subband3, valid floating-point range = {0, 1, 2, 3, ..., 7}
    DSM_API_SETGET_DRC_TRHESHOLD               ,   //32-bit data, Q20, DRC threshold in dB, valid floating-point range = (-120.0, 0.0]
    DSM_API_SETGET_DRC_RATIO                   ,   //32-bit data, Q20, DRC compression ratio,valid floating-point range = [1.0, 1000.0]
    DSM_API_SETGET_DRC_ATTACK                  ,   //32-bit data, Q20, DRC attack time in seconds, valid floating-point range = (0.0, 5.0)
    DSM_API_SETGET_DRC_RELEASE                 ,   //32-bit data, Q20, DRC release time in seconds, valid floating-point range = (0.0, 5.0)
    DSM_API_SETGET_MBDRC_CUTOFF_F1             ,   //32-bit data, Q0,  DRC low-middle subband cutoff frequency in Hz. valid floating-point range = (200, 1000)
    DSM_API_SETGET_MBDRC_CUTOFF_F2             ,   //32-bit data, Q0,  DRC middle-high subband cutoff frequency in Hz. valid floating-point range =(1000, 4000)
    DSM_API_SETGET_MBDRC_DEBUG_MODE            ,   //32-bit data, Q0, DRC internal debug mode. valid floating-point range =[0, 1]      

    DSM_API_SETGET_GUARD_BEEN_MEAN_SCALE       ,   //32-bits data, Q30,                 valid floating-point range = [0, 2.0)

    DSM_API_SETGET_SPEAKER_PARAM_LFX_A1      ,   //32-bits data, Q28,                 valid floating-point range = [0, 8)
    DSM_API_SETGET_SPEAKER_PARAM_LFX_A2      ,   //32-bits data, Q28,                 valid floating-point range = [0, 8)
    DSM_API_SETGET_SPEAKER_PARAM_LFX_B0      ,   //32-bits data, Q28,                 valid floating-point range = [0, 8)
    DSM_API_SETGET_SPEAKER_PARAM_LFX_B1      ,   //32-bits data, Q28,                 valid floating-point range = [0, 8)
    DSM_API_SETGET_SPEAKER_PARAM_LFX_B2      ,   //32-bits data, Q28,                 valid floating-point range = [0, 8)

    DSM_API_SETGET_SPEAKER_PARAM_TCTH1       ,   //32-bits data, Q20,                 valid floating-point range = [0, 2048)
    DSM_API_SETGET_SPEAKER_PARAM_TCTH2       ,   //32-bits data, Q20,                 valid floating-point range = [0, 2048)
    DSM_API_SETGET_SPEAKER_PARAM_RTH1        ,   //32-bits data, Q22,                 valid floating-point range = [0, 512)
    DSM_API_SETGET_SPEAKER_PARAM_RTH2        ,   //32-bits data, Q22,                 valid floating-point range = [0, 512)

    DSM_API_SETGET_SPEAKER_PARAM_ADMIT_A1    ,   //32-bits data, Q28,                 valid floating-point range = [0, 8)
    DSM_API_SETGET_SPEAKER_PARAM_ADMIT_A2    ,   //32-bits data, Q28,                 valid floating-point range = [0, 8)
    DSM_API_SETGET_SPEAKER_PARAM_ADMIT_B0    ,   //32-bits data, Q28,                 valid floating-point range = [0, 8)
    DSM_API_SETGET_SPEAKER_PARAM_ADMIT_B1    ,   //32-bits data, Q28,                 valid floating-point range = [0, 8)
    DSM_API_SETGET_SPEAKER_PARAM_ADMIT_B2    ,   //32-bits data, Q28,                 valid floating-point range = [0, 8)


    DSM_API_SETGET_ENABLE_MULTICHAN_LINKING  ,  //32-bits data, Q0,                  valid floating-point range = {0, 1}

    DSM_API_SETGET_ENABLE_SMART_PT           ,  //32-bits data, Q0,                  valid floating-point range = {0, 1}

    DSM_API_SETGET_ENABLE_LINKWITZ_EQ           ,  //32-bits data, Q0,                 valid floating-point range = {0, 1}

    DSM_API_SETGET_CHANNEL_MASK                 ,  //32-bits data, Q0,                 valid floating-point range = [0, 0x00FF]
    DSM_API_SETGET_ENABLE_FF_FB_MODULES         ,  //32-bits data, Q0: bit0=FF module, bit1=FB module,valid floating-point range = {0, ..., 3}

    DSM_API_SETGET_ENABLE_CROSSOVER             ,  //32-bits data, Q0,                 valid floating-point range = {0, 1}
    DSM_API_SETGET_SPEECH_GUARD_BINS            ,  //32-bits data, Q0,                 valid floating-point range = [0, 0x7FFFFFFF]
    DSM_API_SETGET_MEAN_SPEECH_THRESHOLD        ,  //32-bits data, Q31,                valid floating-point range = [0, 1)

    DSM_API_SETGET_STEREO_CROSSOVER_MODE        ,  //32-bit data, Q0,                  valid floating-point range = {0, 1, 2}
    DSM_API_SETGET_RECEIVER_PHYSICAL_LAYOUT     ,  //32-bit data, Q0,                  valid floating-point range = {LANDSCAPE_1_RCV_RIGHT, LANDSCAPE_2_RCV_LEFT}

    DSM_API_SETGET_XOVER_BOOST_GAIN_PERCENTAGE  ,  //32-bit data, Q27,                 valid floating-point range = [0, 16)

    DSM_API_SETGET_XOVER_FILTER_CUTOFF_FC       ,  //32-bit data, Q9,                  valid floating-point range = [0, 4194304)
    DSM_API_SETGET_XOVER_FILTER_Q               ,  //32-bit data, Q29,                 valid floating-point range = [0, 4)
    DSM_API_SET_XOVER_FILTER_UPDATE             ,  //32-bit data, Q0,                  valid floating-point range = {0, 1}
    DSM_API_SETGET_XOVER_SPKER_GAIN_PERCENTAGE  ,  //32-bit data, Q27,                 valid floating-point range = [0, 16)
};

ADSPResult afe_port_get_dsm_module_params(afe_dev_port_t *pDevPort,uint32_t param_id,
         int8_t* param_buffer_ptr, int32_t param_buf_len, uint16_t* params_buffer_len_req_ptr)
{
    uint32_t result = ADSP_EOK;

    if (RX_DIR != pDevPort->dir )                           //not rx port
    {
        MSG_1(MSG_SSID_QDSP6, DBG_ERROR_PRIO, "Port %x: afe_port_get_dsm_module_params is not Rx.",pDevPort->intf);
        return ADSP_EUNSUPPORTED;
    }

    afe_dsm_module_t   *afe_dsm_ptr = afe_port_get_dsm_module(pDevPort);

    switch (param_id){
    case AFE_PARAM_ID_DSM_CFG:
    {
        if(NULL == afe_dsm_ptr ||
            NULL == param_buffer_ptr)
        {
            MSG(MSG_SSID_QDSP6, DBG_MED_PRIO, "afe_port_get_dsm_module_params fail, not enabled");
            result = ADSP_ENOTREADY;
            break;
        }

        if ((TRUE == afe_dsm_ptr->memory_allocated) &&
            (TRUE == afe_dsm_ptr->resources_allocated))
        {
            int32_t i, pcount = sizeof(getArray);
            unsigned int *param, *pdata = (unsigned int *)param_buffer_ptr;

            if (pcount > 127) pcount = 127;
			*pdata++ = 0;
            *pdata++ = pcount;

            param = pdata;
            for (i = 0; i < pcount; i++)
            {
                *pdata++ = getArray[i] ;
                *pdata++ = 0;
            }

            DSM_API_Get_Params(afe_dsm_ptr->dsm_handle->algo_lib, pcount, param);

            *params_buffer_len_req_ptr = sizeof(int32_t)*(pcount*2 + 2);
              
            MSG_2(MSG_SSID_QDSP6, DBG_FATAL_PRIO,  "got %d parameters param_buf_len %d\n", 
                pcount, param_buf_len);
        }
        else {
            *((uint32_t *)param_buffer_ptr) = 0;
            *((uint32_t *)(param_buffer_ptr + sizeof(uint32_t))) = 0;
            *params_buffer_len_req_ptr = sizeof(uint32_t) * 2;
        }
        break;
    }
    default:
        MSG_2(MSG_SSID_QDSP6, DBG_FATAL_PRIO, "afe_port_get_dsm_module_params: Unsupported PARAM ID: %u, Port_id: %d", (unsigned int)param_id, pDevPort->intf);
        result = ADSP_EUNSUPPORTED;
        break;
    }

    return result;
}


#define DSM_CHANNELS    (1)

/*Create shared algorithm instant and allocate data buffers */
static ADSPResult dsm_algo_init(uint32_t sr, int blk_len)
{
    ADSPResult result = ADSP_EOK;
    dsm_shared_handle_t *dsm_handle = &shared_handle;

    if (0 == qurt_elite_atomic_get(&dsm_handle->ref))
    {
        int ret, lib_size = 0;
        int bufferSpec[10] = {0};

        /*Always initialize algorithm for stereo mode*/
        ret = DSM_API_get_memory_size(DSM_CHANNELS, bufferSpec, &lib_size);

        if (lib_size > 0)
        {
            void *algo_lib = qurt_elite_memory_aligned_malloc(lib_size, 8, QURT_ELITE_HEAP_DEFAULT);
            int8_t *io_buf;

            MSG_2(MSG_SSID_QDSP6, DBG_ERROR_PRIO, "dsm_algo_init at %p with lib size %d\n", algo_lib, lib_size);
            if (algo_lib)
            {
                /*Initialize DSM algorithm*/
                ret = DSM_API_Init(algo_lib, sr, DSM_CHANNELS, bufferSpec, &blk_len);
                if (DSM_API_OK != ret){
                    result = ADSP_ENOTREADY;
                    MSG_3(MSG_SSID_QDSP6, DBG_ERROR_PRIO, "DSM_API_Init sr %d blk_len %d ret 0x%x\n", sr, blk_len, ret);
                    goto fail;
                }

                /*Allocate  IO buffers*/
                lib_size = DSM_CHANNELS*blk_len*sizeof(int16_t);
                io_buf = (int8_t *)qurt_elite_memory_malloc(lib_size, QURT_ELITE_HEAP_DEFAULT);
                if (NULL == io_buf){
                    result = ADSP_ENOMEMORY;
                    MSG_1(MSG_SSID_QDSP6, DBG_ERROR_PRIO, "IO buffer allocate failed with size %d\n", lib_size);
                    goto fail;
                }

                /*Initialize mutex and assign pointers*/
                qurt_elite_mutex_init(&dsm_handle->dsm_algo_mutex);
                dsm_handle->algo_lib = algo_lib;
                dsm_handle->io_buffer = io_buf;
                dsm_handle->block_size = blk_len;
                /*Left only*/
                dsm_handle->channel_mask = 0;
            }
            else
            {
                result = ADSP_ENOMEMORY;
                MSG_1(MSG_SSID_QDSP6, DBG_ERROR_PRIO, "algo lib allocate failed with size %d\n", lib_size);
                goto fail;
            }
        }
    }

fail:
    qurt_elite_atomic_increment(&dsm_handle->ref);

    return result;
}

/**
 * called to init speaker protection algorithm
 */
ADSPResult afe_port_init_dsm(afe_dev_port_t *dev_port)
{
    ADSPResult result = ADSP_EOK;
    afe_dsm_module_t *afe_dsm_ptr;

    afe_dsm_ptr = afe_port_get_dsm_module(dev_port);
    if ( (NULL != afe_dsm_ptr) && (TRUE == afe_dsm_ptr->memory_allocated))
    {
        uint32_t blk_len = 240;
        qurt_elite_mutex_lock(&afe_dsm_ptr->dsm_port_mutex);

        result = dsm_algo_init(dev_port->sample_rate, blk_len);
        if (ADSP_EOK != result)
            goto dsm_init_fail;

        afe_dsm_ptr->dsm_handle = &shared_handle;

        if (afe_dsm_ptr->dsm_handle->pcount > 0 &&
                afe_dsm_ptr->dsm_handle->pcount < DSM_API_SETGET_PARAMS_COUNT)
        {
            DSM_API_Set_Params(afe_dsm_ptr->dsm_handle->algo_lib,
                    afe_dsm_ptr->dsm_handle->pcount,
                    afe_dsm_ptr->dsm_handle->pdata);

            afe_dsm_ptr->dsm_handle->pcount = 0;
        }

        afe_dsm_ptr->dsm_handle->channels = dev_port->channels;
        /*TBD: this is hard coded for 48K sample rate*/
        afe_dsm_ptr->dsm_ff_delay_us = 200;
        
        //create background processing task.
        afe_dsm_ptr->background_proc_ptr = NULL;
        //algorithm operates on block len samples at a time. AFE operates on int_samples_per_period samples at every blk transfer time.
        //algorithm takes > 1ms. this design is for 1 ms blk transfer time &  int_samples_per_period < blk len.
        //also blk len may not be multiple of  int_samples_per_period.

        if (ADSP_FAILED(result = afe_bg_proc_dsm_create(&afe_dsm_ptr->background_proc_ptr, dev_port, \
                        blk_len , afe_dsm_ptr->dsm_handle)))
        {
            MSG_1(MSG_SSID_QDSP6, DBG_ERROR_PRIO, "port %x: spkr_prot Error afe_background_proc_create.",dev_port->intf);
            goto dsm_init_fail;
        }

        afe_dsm_ptr->resources_allocated = TRUE;

    }
    else
    {
        MSG_1(MSG_SSID_QDSP6, DBG_ERROR_PRIO, "port %x: DSM not configured or enabled.",dev_port->intf);
		return ADSP_EOK;
    }

dsm_init_fail:
    qurt_elite_mutex_unlock(&afe_dsm_ptr->dsm_port_mutex);
    return result;
}

/**
 * deinit at port stop: to deinit the speaker protection alg.
 */
ADSPResult afe_port_deinit_dsm(afe_dev_port_t *dev_port)
{
    afe_dsm_module_t *afe_dsm_ptr;
    dsm_shared_handle_t *dsm_handle;

    afe_dsm_ptr = afe_port_get_dsm_module(dev_port);
    if (NULL != afe_dsm_ptr &&
        (DSM_TX_PORT_ID == dev_port->intf || DSM_RX_PORT_ID == dev_port->intf))
    {
        qurt_elite_mutex_lock(&afe_dsm_ptr->dsm_port_mutex);

        dsm_handle = afe_dsm_ptr->dsm_handle;
        qurt_elite_atomic_decrement(&dsm_handle->ref);

        /*reference becomes zero*/
        if (0 == qurt_elite_atomic_get(&dsm_handle->ref) &&
            NULL != dsm_handle)
        {
            if (NULL != dsm_handle->algo_lib)
            {
                qurt_elite_memory_aligned_free(dsm_handle->algo_lib);
                dsm_handle->algo_lib = NULL;
            }

            if (NULL != dsm_handle->io_buffer)
            {
                qurt_elite_memory_free(dsm_handle->io_buffer);
                dsm_handle->io_buffer = NULL;
            }

            qurt_elite_mutex_destroy(&dsm_handle->dsm_algo_mutex);
        }

        afe_dsm_ptr->dsm_handle = NULL;
        afe_dsm_ptr->resources_allocated = FALSE;
        afe_bg_proc_dsm_destroy(afe_dsm_ptr->background_proc_ptr);
        afe_dsm_ptr->background_proc_ptr = NULL;

        qurt_elite_mutex_unlock(&afe_dsm_ptr->dsm_port_mutex);

        if (afe_dsm_ptr->memory_allocated)
        {
            if(dev_port->dir == TX_DIR)
            {
                afe_port_dealloc_resources_for_module(dev_port, AFE_MODULE_DSM_TX);
                afe_port_dealloc_memory_for_module(dev_port, AFE_MODULE_DSM_TX);
            }
            else
            {
                afe_port_dealloc_resources_for_module(dev_port, AFE_MODULE_DSM_RX);
                afe_port_dealloc_memory_for_module(dev_port, AFE_MODULE_DSM_RX);
            }

            afe_dsm_ptr->memory_allocated = FALSE;
        }

        MSG_1(MSG_SSID_QDSP6, DBG_ERROR_PRIO, "afe_port_deinit_dsm for port : 0x%x\n",
            dev_port->intf);
    }
    else if (afe_dsm_ptr)
    {
        MSG_2(MSG_SSID_QDSP6, DBG_ERROR_PRIO, "afe_port_deinit_dsm got wrong pointer %p for port : 0x%x\n",
            afe_dsm_ptr, dev_port->intf);
    }

    return ADSP_EOK;
}


/**
 * populates the output buffer by copying the current content at the end to the top and
 * then reading the algorithm for more data.
 *
 * this is called when algorithm generates some data.
 * The output buffer is big enough to hold all the data that algorithm generates + some factor for jitter.
 *
 * This has to be done under bg proc mutex.
 */
void afe_bg_proc_dsm_populate_out_buf(afe_background_proc_t *bg_proc_ptr, uint16_t num_channels, \
         uint16_t bytes_per_channel)
{
    afe_lin_buffer_t *out_buf_ptr = bg_proc_ptr->out_buf_ptr;
    uint16_t rem_out_sample;

    //if out_buf doesn't have enough valid data, read from algorithm

    //obtain output from the algorithm
    //get num_samples_processed samples or whatever that can hold inside out_buf.
    uint16 frame_len = out_buf_ptr->max_len - out_buf_ptr->actual_len;

    if (frame_len > bg_proc_ptr->num_samples_processed)
    {
        frame_len = bg_proc_ptr->num_samples_processed;
    }

    if (frame_len > 0)
    {
        //call the output function to get more samples.
        rem_out_sample = (out_buf_ptr->max_len - out_buf_ptr->wr_index);

        /* If output buffer has lesser space available and needs to wrap around to write further */
        if (rem_out_sample < frame_len)
        {
            bg_proc_ptr->output_fn(bg_proc_ptr->lib_obj, out_buf_ptr->data + out_buf_ptr->wr_index*bytes_per_channel,
                                rem_out_sample, out_buf_ptr->max_len);

            /* Decrement the frame length by samples already copied */
            frame_len -= rem_out_sample;

            /* Increment the write pointer index */
            out_buf_ptr->wr_index += rem_out_sample;

            /* Decrement the number of samples processed */
            bg_proc_ptr->num_samples_processed -= rem_out_sample;

            /* Increment the actual number of output samples available */
            out_buf_ptr->actual_len += rem_out_sample;

            /* Rest the write counter if it has reached till end of buffer */
            if (out_buf_ptr->wr_index == out_buf_ptr->max_len)
            {
                out_buf_ptr->wr_index = 0;
            }

            //MSG(MSG_SSID_QDSP6, DBG_FATAL_PRIO, "[MAXIM]: INSIDE if (rem_out_sample < frame_len)");
        }

        /* Copy the partial or complete frame */
        bg_proc_ptr->output_fn(bg_proc_ptr->lib_obj, out_buf_ptr->data + out_buf_ptr->wr_index*bytes_per_channel,
            frame_len, out_buf_ptr->max_len);

        /* Decrement the number of samples processed */
        bg_proc_ptr->num_samples_processed  -= frame_len;

        /* Increment the number of output samples available */
        out_buf_ptr->actual_len             += frame_len;

        /* Increment the write index */
        out_buf_ptr->wr_index += frame_len;

        /* Wrap around the write index if it has reached end of buffer */
        if (out_buf_ptr->wr_index == out_buf_ptr->max_len)
        {
            out_buf_ptr->wr_index = 0;
        }
    }
    else
    {
        //this means num_samples_processed is zero or max_len==actual_len. latter doesn't happen usually.
        MSG_2(MSG_SSID_QDSP6, DBG_ERROR_PRIO, "afe bg_proc: frame_len == 0. try increasing AFE_BG_PROC_ALG_PROC_DELAY_JITTER_MS if actual_len(%d)!=max_len(%d) or MIPs.",out_buf_ptr->actual_len,out_buf_ptr->max_len);
    }
}

void afe_dsm_process(afe_dev_port_t *dev_port, int8_t* afe_in_buffer, int8_t* afe_out_buffer)
{
    afe_dsm_module_t *afe_dsm_ptr;
    afe_background_proc_t *bg_proc_ptr;
    afe_lin_buffer_t *in_ping_buf_ptr;
    afe_lin_buffer_t *out_buf_ptr;    
    uint32_t in_buf_max_size, out_buf_max_size; 
    uint16_t chan, rem_out_sample; 
    uint16_t num_bytes_in_channel           = dev_port->bytes_per_channel * dev_port->int_samples_per_period;
    uint16_t num_channels                   = dev_port->channels;
    uint16_t num_samples_in_channel         = dev_port->int_samples_per_period;
    uint16_t port_buf_chan_spacing_in_bytes = dev_port->bytes_per_channel * dev_port->int_samples_per_period;

    afe_dsm_ptr = afe_port_get_dsm_module(dev_port);
    if (afe_dsm_ptr && afe_dsm_ptr->background_proc_ptr &&
        afe_dsm_ptr->resources_allocated)
    {
        bg_proc_ptr         = afe_dsm_ptr->background_proc_ptr;
        in_ping_buf_ptr     = bg_proc_ptr->in_buf_ping_ptr;
        out_buf_ptr         = bg_proc_ptr->out_buf_ptr;
        in_buf_max_size     = in_ping_buf_ptr->max_len * dev_port->bytes_per_channel;
        out_buf_max_size    = out_buf_ptr->max_len * dev_port->bytes_per_channel;
    }
    else
    {
        return;
    }
    
   
   qurt_elite_mutex_lock(&bg_proc_ptr->mutex);

   if (in_ping_buf_ptr->wr_index >= in_ping_buf_ptr->max_len)
   {
    MSG_1(MSG_SSID_QDSP6, DBG_ERROR_PRIO, "Input buffer overrun with write index %d, will drop current frame\n",
                in_ping_buf_ptr->wr_index);
    
    goto out;
   }
   
   //copy the incoming samples into in_buf
   for (chan = 0; chan < num_channels; chan++)
   {
      /*memscpy(in_ping_buf_ptr->data + (in_ping_buf_ptr->wr_index*dev_port->bytes_per_channel) + chan * in_buf_max_size,
              (in_ping_buf_ptr->max_len - in_ping_buf_ptr->wr_index) * dev_port->bytes_per_channel,
               afe_in_buffer + chan * num_bytes_in_channel,
               num_bytes_in_channel);*/
         memcpy(in_ping_buf_ptr->data + (in_ping_buf_ptr->wr_index*dev_port->bytes_per_channel) + chan * in_buf_max_size,
               afe_in_buffer + chan * num_bytes_in_channel,
               num_bytes_in_channel);              
   }

   
    in_ping_buf_ptr->wr_index                            += dev_port->int_samples_per_period;
    in_ping_buf_ptr->actual_len                          += dev_port->int_samples_per_period;
    bg_proc_ptr->num_samples_to_be_processed             += dev_port->int_samples_per_period;
   
    //if alg is not processed even once, then fill zeros to output.
    if ((bg_proc_ptr->is_first_bg_proc) && (RX_DIR == dev_port->dir))
    {
        memset(afe_out_buffer, 0, num_bytes_in_channel * num_channels);
    }
    else if (RX_DIR == dev_port->dir)
    {
        /* If out_buf doesn't have enough valid data, set the current output to zero */
        if (out_buf_ptr->actual_len < dev_port->int_samples_per_period)
        {
            MSG_2(MSG_SSID_QDSP6, DBG_ERROR_PRIO, "afe bg_proc: underrun, num out samples [%u] less than int_samples_per_period [%lu]",
                out_buf_ptr->actual_len, dev_port->int_samples_per_period);

            /* Copy the available data to output buffer */
            for (chan = 0; chan < num_channels; chan++)
            {
                memcpy(afe_out_buffer + (chan * port_buf_chan_spacing_in_bytes), \
                    out_buf_ptr->data + out_buf_ptr->rd_index*dev_port->bytes_per_channel + chan * out_buf_max_size,\
                    out_buf_ptr->actual_len * dev_port->bytes_per_channel);
                   
                /* Increment the port output buffer pointer by samples already written */
                afe_out_buffer += (out_buf_ptr->actual_len * dev_port->bytes_per_channel);

                /* Calculate the number of sample remain to be copied in the port output buffer */
                rem_out_sample = (dev_port->int_samples_per_period - out_buf_ptr->actual_len);

                /* Zero out the remaining portion in output buffer */
                memset(afe_out_buffer + (chan * port_buf_chan_spacing_in_bytes), 0, rem_out_sample * dev_port->bytes_per_channel);

                /* Increment the read pointer by length already read */
                out_buf_ptr->rd_index += out_buf_ptr->actual_len;

                /* Reset the output length as all the data has been consumed */
                out_buf_ptr->actual_len = 0;

                /* Reset the read counter if it has reached till end */
                if (out_buf_ptr->rd_index == out_buf_ptr->max_len)
                {
                    out_buf_ptr->rd_index = 0;
                }
            }
        }
        else /* If enough samples are available for port buffer */
        {
            //copy output samples from bg_proc_ptr->out_buf to afe_out_buffer
            rem_out_sample = (out_buf_ptr->max_len - out_buf_ptr->rd_index);

            /* If read pointer is such that there are not enough samples and it needs to wrap around */
            if (rem_out_sample < dev_port->int_samples_per_period)
            {
                num_bytes_in_channel = rem_out_sample * dev_port->bytes_per_channel;

                for (chan = 0; chan < num_channels; chan++)
                {
                    memcpy(afe_out_buffer + chan * port_buf_chan_spacing_in_bytes, \
                        out_buf_ptr->data + out_buf_ptr->rd_index*dev_port->bytes_per_channel + chan * out_buf_max_size,\
                        num_bytes_in_channel);                      
                }

                /* Reset the read index to start of the buffer */
                out_buf_ptr->rd_index = 0;

                /* Decrement the data length already read */
                out_buf_ptr->actual_len -= rem_out_sample;

                /* Advance the write pointer with num bytes already written */
                afe_out_buffer += num_bytes_in_channel;

                /* Calculate number of samples remaining to be copied */
                num_samples_in_channel = (dev_port->int_samples_per_period - rem_out_sample);

                /* Calculate corresponding number of bytes remaining to be copied */
                num_bytes_in_channel = num_samples_in_channel * dev_port->bytes_per_channel;
            }

            /* Read the partial or complete frame */
            for (chan = 0; chan < num_channels; chan++)
            {
                memcpy(afe_out_buffer + chan * port_buf_chan_spacing_in_bytes, \
                    out_buf_ptr->data + out_buf_ptr->rd_index*dev_port->bytes_per_channel + chan * out_buf_max_size,\
                    num_bytes_in_channel);              
            }
        
            out_buf_ptr->rd_index    += num_samples_in_channel;
            out_buf_ptr->actual_len  -= num_samples_in_channel;

            /* Reset the read counter if it has reached till end of lib output buffer */
            if (out_buf_ptr->rd_index == out_buf_ptr->max_len)
            {
                out_buf_ptr->rd_index = 0;
            }
        } /* If enough samples available for port buffer to render */
    }

    //if enough input samples are collected, invoke background processing by signaling, if last process has gone through
    if ((bg_proc_ptr->num_samples_to_be_processed >= bg_proc_ptr->alg_blk_len) &&
        (bg_proc_ptr->is_last_process_done))
    {
        //swap ping pong buffers.
        afe_lin_buffer_t *temp              = bg_proc_ptr->in_buf_ping_ptr;
        bg_proc_ptr->in_buf_ping_ptr        = bg_proc_ptr->in_buf_pong_ptr;
        bg_proc_ptr->in_buf_pong_ptr        = temp;
        bg_proc_ptr->is_last_process_done   = FALSE;
        bg_proc_ptr->num_samples_to_be_processed  -= bg_proc_ptr->alg_blk_len;

        bg_proc_ptr->dynamic_thread_ptr->isr_context_counter++;
        qurt_elite_signal_send(&bg_proc_ptr->invoke_signal);
    }
    else
    {
        if (bg_proc_ptr->num_samples_to_be_processed > (in_ping_buf_ptr->max_len + dev_port->int_samples_per_period) )
        {
            MSG(MSG_SSID_QDSP6, DBG_ERROR_PRIO, "afe DSM dma: underrun. MIPs issue.");
        }
    }
   
out:   
    qurt_elite_mutex_unlock( &bg_proc_ptr->mutex);

}

/**
 * called from background processing thread.
 *
 * called from low prio threadpool.
 *
 * steps:
 * -call algorithm process function by passing all the samples we have.
 * -algorithm processes alg_blk_len samples. the rest are stored in the algo. num_samples_to_be_processed helps in keeping track this.
 * -read the processed alg_blk_len samples from alg or read those many samples that we can hold in out_buf.
 *
 * this function operates on pong buffer
 */
ADSPResult afe_bg_proc_dsm_bg_process(void *bg_proc_void)
{
    afe_background_proc_t *bg_proc_ptr  = (afe_background_proc_t*) bg_proc_void;
    afe_dev_port_t        *dev_port     = bg_proc_ptr->dev_port_ptr;
    uint16_t num_channels               = dev_port->channels;
    afe_lin_buffer_t *in_pong_buf_ptr   = bg_proc_ptr->in_buf_pong_ptr;
    afe_dsm_module_t  *afe_dsm_ptr;

    afe_dsm_ptr = afe_port_get_dsm_module(dev_port);
    if (NULL == afe_dsm_ptr)
        return ADSP_EHANDLE;
   
    // mutex protection for speaker protection module resources w.r.t START and STOP cmds
    qurt_elite_mutex_lock(&afe_dsm_ptr->dsm_port_mutex);

    // do the back ground processing only when the module is active
    if (TRUE == afe_dsm_ptr->resources_allocated)
    {
        //MSG(MSG_SSID_QDSP6, DBG_FATAL_PRIO, "[MAXIM]: afe bg_proc: afe_bg_proc_bg_process start");
   
        //pass all the samples (in_buf_actual_len) we have although algorithm can process only alg_blk_len at a time.
        bg_proc_ptr->process_fn(bg_proc_ptr->lib_obj, in_pong_buf_ptr->data,
                                in_pong_buf_ptr->actual_len, in_pong_buf_ptr->max_len);

        // increase the priority of back ground processing thread same as thread pool thread to 
        // reduce wait time for mutex on thread pool thread
        qurt_elite_thread_set_prio(ELITETHREAD_STAT_AFE_PORT_MANAGER_PRIO);

        qurt_elite_mutex_lock( &bg_proc_ptr->mutex);

        bg_proc_ptr->is_last_process_done = TRUE;

        //change wr index so that incoming data is written at the right place in dma process fn.
        in_pong_buf_ptr->wr_index     = 0;
        in_pong_buf_ptr->actual_len   = 0;

        bg_proc_ptr->is_first_bg_proc             = FALSE;
        bg_proc_ptr->num_samples_processed        += bg_proc_ptr->alg_blk_len;

        //get the output samples. mutex needed because DMA processing thread may be trying to read samples from out_buf.
        if(TX_DIR != dev_port->dir) // Skip output buffer copy in case of IV record
        {
            afe_bg_proc_dsm_populate_out_buf(bg_proc_ptr, num_channels, dev_port->bytes_per_channel);
        }

        qurt_elite_mutex_unlock( &bg_proc_ptr->mutex);

        // bring it back to normal back ground thread pool thread priority
        qurt_elite_thread_set_prio(ELITETHREAD_CVS_MED_PRIO);

        /*MSG_4(MSG_SSID_QDSP6, DBG_ERROR_PRIO, "afe bg_proc:bg: out_buf_ptr->actual_len=%d,in_buf=%d,to_be_processed=%d,processed=%d",\
                    out_buf_ptr->actual_len,in_pong_buf_ptr->actual_len,bg_proc_ptr->num_samples_to_be_processed,bg_proc_ptr->num_samples_processed);*/

        //MSG(MSG_SSID_QDSP6, DBG_FATAL_PRIO, "[MAXIM]: afe bg_proc: afe_bg_proc_bg_process end");
    }

    qurt_elite_mutex_unlock(&afe_dsm_ptr->dsm_port_mutex);

    return ADSP_EOK;
}

/**
 * below function creates a background processing task , inits and adds it to threadpool.
 *
 */
static ADSPResult afe_bg_proc_dsm_create(afe_background_proc_t **bg_proc_ptr_ptr,
         afe_dev_port_t *dev_port,
         uint16_t alg_blk_len,
         void *lib_obj)
{
    uint16_t                   int_samples_per_period  = dev_port->int_samples_per_period;
    uint16_t                   num_chan                = dev_port->channels;
    afe_background_proc_t      *bg_proc_ptr;
    afe_dynamic_thread_task_t  task;
    ADSPResult                 result = ADSP_EOK;

    *bg_proc_ptr_ptr = NULL;

    if (NULL == (bg_proc_ptr = (afe_background_proc_t*)qurt_elite_memory_malloc(sizeof(afe_background_proc_t), QURT_ELITE_HEAP_DEFAULT)) )
    {
        MSG(MSG_SSID_QDSP6, DBG_ERROR_PRIO, "afe bg_proc: Failed to allocate afe_background_proc_t");
        return ADSP_ENOMEMORY;
    }

    memset(bg_proc_ptr, 0, sizeof(afe_background_proc_t));

    *bg_proc_ptr_ptr = bg_proc_ptr;

    if (int_samples_per_period > alg_blk_len)
    {
        MSG(MSG_SSID_QDSP6, DBG_ERROR_PRIO, "afe bg_proc: int_samples_per_period > alg_blk_len not supported as of now.");
        return ADSP_EUNSUPPORTED;
    }

    //need extra space in the buffers
    //because blk len from algorithm & int_samples_per_period are not multiple of each other.

    //buffers will be of integer multiple of int_samples_per_period greater than alg_blk_len
    uint16_t int_blk_len = ((alg_blk_len / int_samples_per_period)) * int_samples_per_period;

    bg_proc_ptr->in_buf_ping_ptr   = &bg_proc_ptr->in_buf_ping;
    bg_proc_ptr->in_buf_pong_ptr   = &bg_proc_ptr->in_buf_pong;
    bg_proc_ptr->out_buf_ptr       = &bg_proc_ptr->out_buf;

    bg_proc_ptr->in_buf_ping.actual_len = 0;
    bg_proc_ptr->in_buf_ping.max_len    = int_blk_len;
    bg_proc_ptr->in_buf_ping.rd_index   = 0;
    bg_proc_ptr->in_buf_ping.wr_index   = 0;

    bg_proc_ptr->in_buf_pong.actual_len = 0;
    bg_proc_ptr->in_buf_pong.max_len    = int_blk_len;
    bg_proc_ptr->in_buf_pong.rd_index   = 0;
    bg_proc_ptr->in_buf_pong.wr_index   = 0;

    bg_proc_ptr->out_buf.actual_len     = 0;       /* Read is advanced by half the buffer size */
    bg_proc_ptr->out_buf.max_len        = int_blk_len * 2;   /* Ping pong buffer for the output */
    bg_proc_ptr->out_buf.rd_index       = 0;
    bg_proc_ptr->out_buf.wr_index       = 0;

    bg_proc_ptr->alg_blk_len                    = alg_blk_len ;
    bg_proc_ptr->num_samples_to_be_processed    = 0;
    bg_proc_ptr->num_samples_processed          = 0;

    qurt_elite_mutex_init(&bg_proc_ptr->mutex);

    if (ADSP_FAILED(result = qurt_elite_signal_init(&(bg_proc_ptr->invoke_signal))))
    {
        MSG_1(MSG_SSID_QDSP6, DBG_ERROR_PRIO, "afe bg_proc: ADSP_FAILED to init signal  = %d!!\n", result);
        return result;
    }

    task.task_signal_ptr = &bg_proc_ptr->invoke_signal;
    task.pfn_task_func   = afe_bg_proc_dsm_bg_process;
    task.vp_task_arg     = (void*)bg_proc_ptr;
    if (ADSP_SUCCEEDED(result = afe_dynamic_thread_launch(&bg_proc_ptr->dynamic_thread_ptr, &task, 
        ELITETHREAD_CVS_MED_PRIO, AFE_THREAD_STACK_SIZE, (uint32)dev_port->intf)))
    {
        MSG_1(MSG_SSID_QDSP6, DBG_HIGH_PRIO, "afe bg_proc: Adding task %p", bg_proc_ptr->dynamic_thread_ptr);
    }
    else
    {
        MSG(MSG_SSID_QDSP6, DBG_ERROR_PRIO, "afe bg_proc: Failed to add task.");
        return result;
    }

    //sum all buffer needs
    uint32_t size = (bg_proc_ptr->in_buf_ping.max_len +
                    bg_proc_ptr->in_buf_pong.max_len +
                    bg_proc_ptr->out_buf.max_len) *
                    (dev_port->bytes_per_channel * num_chan);

    if (NULL == (bg_proc_ptr->in_buf_ping.data = (int8_t*)qurt_elite_memory_malloc(size, QURT_ELITE_HEAP_DEFAULT)) )
    {
        MSG(MSG_SSID_QDSP6, DBG_ERROR_PRIO, "afe bg_proc: Failed to allocate afe_background_proc_t in_buf");
        return ADSP_ENOMEMORY;
    }
    memset(bg_proc_ptr->in_buf_ping.data, 0, size);

    bg_proc_ptr->in_buf_pong.data = bg_proc_ptr->in_buf_ping.data + (bg_proc_ptr->in_buf_ping.max_len) * (dev_port->bytes_per_channel * num_chan);
    bg_proc_ptr->out_buf.data     = bg_proc_ptr->in_buf_pong.data + (bg_proc_ptr->in_buf_pong.max_len) * (dev_port->bytes_per_channel * num_chan);

    MSG_4(MSG_SSID_QDSP6, DBG_ERROR_PRIO, "size %d, ping %p, pong %p, output %p", size, bg_proc_ptr->in_buf_ping_ptr->data,
        bg_proc_ptr->in_buf_pong_ptr->data, bg_proc_ptr->out_buf_ptr->data);

    if (TX_DIR == dev_port->dir)
    {
        bg_proc_ptr->process_fn      = dsm_process_fn_tx;
        bg_proc_ptr->output_fn       = dsm_output_fn_tx;
    }
    else
    {
        bg_proc_ptr->process_fn      = dsm_process_fn_rx;
        bg_proc_ptr->output_fn       = dsm_output_fn_rx;
    }

    bg_proc_ptr->lib_obj                = lib_obj;
    bg_proc_ptr->is_first_bg_proc       = TRUE;
    bg_proc_ptr->is_last_process_done   = TRUE; //assume process has been done once
    bg_proc_ptr->dev_port_ptr           = dev_port;


    return result;
}

static void afe_bg_proc_dsm_destroy(afe_background_proc_t *bg_proc_ptr)
{
    if (NULL != bg_proc_ptr)
    {
        if (NULL != bg_proc_ptr->in_buf_ping.data)
        {
            qurt_elite_memory_free(bg_proc_ptr->in_buf_ping.data);
        }

        if (NULL != bg_proc_ptr->dynamic_thread_ptr)
        {
            afe_dynamic_thread_destroy(&bg_proc_ptr->dynamic_thread_ptr);
        }

        qurt_elite_mutex_destroy(&bg_proc_ptr->mutex);

        qurt_elite_memory_free(bg_proc_ptr);
    }
}

static void dsm_process_fn_rx (void *lib_obj, int8_t* in_buffer, int16 frame_size, int16 channel_spacing)
{   
    dsm_shared_handle_t *dsm_handle = (dsm_shared_handle_t *)lib_obj;
    int sr = dsm_handle->block_size *  DSM_CHANNELS;

    /*Mix left and right channel together for stereo stream*/
    if (2 == dsm_handle->channels) {
        afe_port_stereo2mono_by_avg(in_buffer, in_buffer + (channel_spacing<< 1), in_buffer, 
            dsm_handle->block_size, sizeof(short));
    }
    
    DSM_API_FF_process(dsm_handle->algo_lib, dsm_handle->channel_mask,
        (short *)in_buffer, &sr, (short *)dsm_handle->io_buffer, &sr);
}

//static short tmpV[240], tmpI[240];

static void dsm_process_fn_tx (void *lib_obj, int8_t* in_buffer, int16 frame_size, int16 channel_spacing)
{   
    dsm_shared_handle_t *dsm_handle = (dsm_shared_handle_t *)lib_obj;
    int sr =  dsm_handle->block_size * DSM_CHANNELS;
    short *current = (short *) in_buffer;
    short *voltage = current + channel_spacing;

    DSM_API_FB_process(dsm_handle->algo_lib, dsm_handle->channel_mask,
        current, voltage, &sr);

    //debug codes here
    if (0)
    {
        static int count = 0;

        count++;

        if (count >= 100)
        {
            unsigned int values[15] = {
                DSM_API_GET_ADAPTIVE_FC, 0, 0,
                //DSM_API_GET_ADAPTIVE_Q, 0, 0,
                DSM_API_GET_ADAPTIVE_DC_RES, 0, 0,
                //DSM_API_GET_ADAPTIVE_COILTEMP, 0, 0,
                //DSM_API_GET_EXCURSION, 0, 0
            };

            DSM_API_Get_Params(dsm_handle->algo_lib, 2, values);

            MSG_4(MSG_SSID_QDSP6, DBG_HIGH_PRIO, "fc [%d, %d]  rdc [%d, %d]\n",
                values[1], values[2], values[4], values[5]);

            count = 0;
        }
    }
}

static void dsm_output_fn_rx (void *lib_obj, int8_t* out_buffer, int16 frame_size, int16 channel_spacing) 
{
    dsm_shared_handle_t *dsm_handle = (dsm_shared_handle_t *)lib_obj;
    
    memscpy(out_buffer, frame_size<<1, dsm_handle->io_buffer, channel_spacing<<1);
}

static void dsm_output_fn_tx (void *lib_obj, int8_t* out_buffer, int16 frame_size, int16 channel_spacing) 
{
    //dsm_shared_handle_t *dsm_handle = (dsm_shared_handle_t *)lib_obj;
    //memscpy(out_buffer, channel_spacing<<1, tmpI, frame_size<<1);
    //memscpy(out_buffer + (channel_spacing<<1), channel_spacing<<1, tmpV, frame_size<<1);
}
#endif

