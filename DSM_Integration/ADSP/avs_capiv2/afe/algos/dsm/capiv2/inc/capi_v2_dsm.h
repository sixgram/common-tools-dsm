/* ======================================================================== */
/**
   @file capi_v2_dsm.h

   C header file to implement CAPIv2 DSM module
 */

/* =========================================================================
   Copyright (c) 2015 Maxim Integrated Products. All rights reserved.
   ========================================================================= */
#ifndef CAPI_V2_DSM_H_
#define CAPI_V2_DSM_H_
#include "mmdefs.h"
#include "Elite_CAPI_V2_properties.h"
#include "Elite_CAPI_V2.h"


#ifdef __cplusplus
extern "C"
{
#endif

// The numbers corresponding to these macros are just placeholders

/* Parameter ID to enable DSM module.
 * Module is enabled by setting the flag to 1
 */
//#define CAPI_V2_PARAM_ID_DSM_ENABLE	0x00012D13

#define  DSM_FEED_FORWARD_PROCESS  1
#define  DSM_FEED_BACK_PROCESS     2
#define  LOG_BUFFER_ARRAY_SIZE     10

/** @brief Structure for setting the configuration parameters for the
    DSM module.
 */
typedef struct dsm_filter_set_params_t dsm_filter_set_params_t;
struct dsm_filter_set_params_t {
    /** \ Sequence of DSM filter parameters */
    uint32_t dcResistance;    //Q27, read only
    uint32_t coilTemp;        //Q19, read only
    uint32_t qualityfactor;   //Q29, read only
    uint32_t resonanceFreq;   //Q9,  read only
    uint32_t excursionMeasure;  //Q0, integer representing a %
    /** \ sequence of control parameters */
    uint32_t rdcroomtemp;        //Q27
    uint32_t releasetime;        //Q30
    uint32_t coilthermallimit;   //Q19
    uint32_t excursionlimit;     //Q27
    uint32_t dsmenabled;         //Q0
    uint32_t staticgain;         //Q29
    uint32_t lfxgain;            //Q30
    uint32_t pilotgain;          //Q31
    uint32_t flagToWrite;        //Q0
    uint32_t featureSetEnable;   //Q0
    uint32_t smooFacVoltClip;
    uint32_t highPassCutOffFactor; //Q9
    uint32_t leadResistance;       //Q27
    uint32_t rmsSmooFac;
    uint32_t clipLimit;          //Q27
    uint32_t thermalCoeff;       //Q20
    uint32_t qSpk;               //Q29
//#ifdef LOG_TEMP_EXCUR_DATA
    uint32_t excurLoggingThresh;    //Q0
    uint32_t coilTempLoggingThresh; //Q0
//#endif
    uint32_t resFreq;          //Q9
    uint32_t resFreqGuardBand; //This param is not exposed
    uint32_t ambientTemp;      //Q19
    uint32_t STL_attack_time;  //Q19, read only
    uint32_t STL_release_time; //Q19, read only
    uint32_t STL_a1;           //Q30
    uint32_t STL_a2;           //Q30
    uint32_t STL_b0;           //Q30
    uint32_t STL_b1;           //Q30
    uint32_t STL_b2;           //Q30
    uint32_t Tch1;             //Q20
    uint32_t Rth1;             //Q24
    uint32_t Tch2;             //Q20
    uint32_t Rth2;             //Q23
    uint32_t STL_Attenu_Gain;  //Q30, read only
    uint32_t SPT_rampDownFrames;  //Q0
    uint32_t SPT_Threshold;       //Q0
    uint32_t T_horizon;         //This parameter is not exposed
    uint32_t LFEx_a1;           //Q28
    uint32_t LFEx_a2;           //Q28
    uint32_t LFEx_b0;           //Q28
    uint32_t LFEx_b1;           //Q28
    uint32_t LFEx_b2;           //Q28
    uint32_t X_max;             //Q15
    uint32_t spk_fs;            //This parameter is not exposed
    uint32_t q_guard_band;      //This parameter does not exist, there is a hard-coded 0.5 factor
    uint32_t STImpedModel_a1;   //Q30
    uint32_t STImpedModel_a2;   //Q30
    uint32_t STImpedModel_b0;   //Q27
    uint32_t STImpedModel_b1;   //Q27
    uint32_t STImpedModel_b2;   //Q27
    uint32_t STImpedModel_Flag; //This parameter does not exist
    uint32_t Q_Notch;           //This parameter does not exist
//    uint32_t Reserve_0;
//    uint32_t Reserve_1;
//    uint32_t Reserve_2;
//    uint32_t Reserve_3;
//    uint32_t Reserve_4;
};

/** @brief Structure for getting the parameters for the
 *  DSM module.
 */
typedef struct dsm_filter_get_params_t dsm_filter_get_params_t;
struct dsm_filter_get_params_t {
	  /** \ Sequence of DSM filter parameters */
    uint32_t dcResistance;
    uint32_t coilTemp;
    uint32_t qualityfactor;
    uint32_t resonanceFreq;
    uint32_t excursionMeasure;
   /** \ sequence of control parameters */
    uint32_t rdcroomtemp;
    uint32_t releasetime;
    uint32_t coilthermallimit;
    uint32_t excursionlimit;
    uint32_t dsmenabled;
    uint32_t staticgain;
    uint32_t lfxgain;
    uint32_t pilotgain;
    uint32_t flagToWrite;
    uint32_t featureSetEnable;
    uint32_t smooFacVoltClip;
    uint32_t highPassCutOffFactor;
    uint32_t leadResistance;
    uint32_t rmsSmooFac;
    uint32_t clipLimit;
    uint32_t thermalCoeff;
    uint32_t qSpk;
//#ifdef LOG_TEMP_EXCUR_DATA
    uint32_t excurLoggingThresh;
    uint32_t coilTempLoggingThresh;
//#endif
    uint32_t resFreq;
    uint32_t resFreqGuardBand;
    uint32_t ambientTemp;
    uint32_t STL_attack_time;
    uint32_t STL_release_time;
    uint32_t STL_a1;
    uint32_t STL_a2;
    uint32_t STL_b0;
    uint32_t STL_b1;
    uint32_t STL_b2;
    uint32_t Tch1;
    uint32_t Rth1;
    uint32_t Tch2;
    uint32_t Rth2;
    uint32_t STL_Attenu_Gain;  //Q30
    uint32_t SPT_rampDownFrames;
    uint32_t SPT_Threshold;
    uint32_t T_horizon;  //Q0
    uint32_t LFEx_a1;
    uint32_t LFEx_a2;
    uint32_t LFEx_b0;
    uint32_t LFEx_b1;
    uint32_t LFEx_b2;
    uint32_t X_max;
    uint32_t spk_fs;
    uint32_t q_guard_band;
    uint32_t STImpedModel_a1;
    uint32_t STImpedModel_a2;
    uint32_t STImpedModel_b0;
    uint32_t STImpedModel_b1;
    uint32_t STImpedModel_b2;
    uint32_t STImpedModel_Flag;
    uint32_t Q_Notch;
    uint32_t power_measurement;
    uint32_t Reserve_1;
    uint32_t Reserve_2;
    uint32_t Reserve_3;
    uint32_t Reserve_4;
//#ifdef LOG_TEMP_EXCUR_DATA
//#ifdef ENABLE_GET_LOG_DATA
    /* Refer "byteLogStruct" for mapping below array */
    uint8_t  byteLogArray[4+(LOG_BUFFER_ARRAY_SIZE*2)];
    /* Refer "intLogStruct" for mapping below array */
    uint32_t intLogArray[4+(LOG_BUFFER_ARRAY_SIZE*2)];
    /* Refer "byteLogStruct" for mapping below array */
    uint8_t  afterProbByteLogArray[LOG_BUFFER_ARRAY_SIZE*2*2];
    /* Refer "intLogStruct" for mapping below array */
    uint32_t afterProbIntLogArray[LOG_BUFFER_ARRAY_SIZE*2*2];
//#endif
//#endif
};

#define DSM_PARAMS_TOTAL_LEN (248)
typedef struct dsm_param_array_t dsm_param_array_t;
struct dsm_param_array_t {
    uint32_t    mode;                   //High 16
    uint32_t    count;
    uint32_t    flagToWrite;
    uint32_t    Reserve;
    uint32_t    params[DSM_PARAMS_TOTAL_LEN];
};

typedef struct dsm_get_runtime_params_t dsm_get_runtime_params_t;
struct dsm_get_runtime_params_t {
    int32_t dc_resistance;
    int32_t quality_factor;
    int32_t resonance_freq;  //Q9
    int32_t coil_temp;
};

capi_v2_err_t capi_v2_dsm_get_static_properties (
        capi_v2_proplist_t *init_set_properties,
        capi_v2_proplist_t *static_properties);
capi_v2_err_t capi_v2_dsm_init (capi_v2_t* _pif,
        capi_v2_proplist_t      *init_set_properties);

capi_v2_err_t capi_v2_dsm_iv_get_static_properties(
        capi_v2_proplist_t *init_set_properties,
        capi_v2_proplist_t *static_properties);
capi_v2_err_t capi_v2_dsm_iv_init(capi_v2_t* _pif, capi_v2_proplist_t *init_set_properties);

#ifdef __cplusplus
}
#endif

#endif /* CAPI_V2_DSM_H_ */
